import moment from 'moment';
import geolib from 'geolib';
export let getAroundOrders = (orders,lat,lng,distanc)=>{
    
    console.log(' i am here and gonna check the nearest orders to you', orders , lat , lng);
    
    let Ordersdistances =orders.map((order , index)=>{

        let orderDistance = geolib.getDistance(
            {latitude: lat, longitude: lng},
            {latitude: order.lat, longitude: order.lng}
        );
        return {...order , orderDistance}
    })
    
    console.log('Ordersdistances : ', Ordersdistances);
    return Ordersdistances.filter((order)=>{
        return order.orderDistance <= distanc;
    })
}
export default (orders, { text, sortBy, startDate, endDate },user = undefined,admin=undefined) => {
    console.log('in visual orders : user is ', user,'admin : ', admin);
    
    return orders.filter((order) => {
        // if the startDate is undefined it will result in true ,, if the created at is bigger than the start date filter it will result in true 
        const momentCreatedAt = moment(Number(order.createdAt));
        const startDateMatch = startDate ? startDate.isSameOrBefore(momentCreatedAt , 'day'): true
        const endDateMatch = endDate ? endDate.isSameOrAfter(momentCreatedAt , 'day'): true
        const textMatch = order.serviceType.map((service)=>{
            if(service.toLowerCase().includes(text.toLowerCase())){
                return true
            }else {
                return false
            }
        })
        let userMatch = undefined;
        user === undefined ? userMatch = false : userMatch = order.user.email === user.email  && user.auth !== undefined ? true : false;
        if(admin){
            console.log(' inside the admin');
            return startDateMatch && endDateMatch && textMatch ;
        }else{
            console.log(' didnt go to admin : usermatch = ',userMatch ,startDateMatch && endDateMatch && textMatch)
            return startDateMatch && endDateMatch && textMatch  && userMatch// if any of these results in true , the ex wont be returned .
        }
        
    }).sort((a, b) => {
        if (sortBy === 'Date') {
            console.log(' i am in sort by date',a.createdAt , b.createdAt);
            return a.createdAt < b.createdAt ? 1 : -1
        } else if (sortBy === 'Amount') {
            console.log(' i am in sort by amount', a.amount , b.amount);
            return a.amount < b.amount ? 1 : -1
        }
    })
}

