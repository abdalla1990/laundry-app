//admin is an array containes admoin users
export default (admin) =>{
    return admin.filter((admin)=>{
        return admin.validate === true && admin.auth !== undefined
    });
}
