import uuid from 'uuid';

export const addAdmin =
    ({id = '', firstName = '', lastName = '', email = '', password = '' ,password2  ='' } = {}) =>
    ({
        type: 'ADD_ADMIN',
        user: {
            id: uuid(),
            firstName,
            lastName,
            email,
            password,
            password2
            
        }
    });
    export const removeAdmin =
        ({ email } = {}) =>
        ({
            type: 'REMOVE_ADMIN',
            admin: { email }
        });

    export const editAdmin=
        (id, updates) =>
        ({
            type: 'EDIT_ADMIN',
            id,
            updates
        });

    export const validateAdmin =
    (userCredintials) =>
    ({
        type: 'VALIDATE_ADMIN',
        userCredintials
    })