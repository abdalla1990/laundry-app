

export const toggleMenu =
    (id) =>
    ({
        type: 'TOGGLE_MENU',
        id
    });

export const unToggleItem =
    (id) =>
    ({
        type: 'UNTOGGLE_MENU',
        id
    });


