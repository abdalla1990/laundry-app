import uuid from 'uuid';
import {getOrders} from '../add-edit-order-handler/addEditOrderHandler';


const postOrders = (orders)=>({type:'POST_ORDERS',orders});

const clearOrders = ()=>{  console.log('DISPATCHING') ;return {type:'CLEAR_ORDERS'}}

export const fetchUserOrders = (userId) => {
    return  (dispatch , getState) => {
        return getOrders(userId).then((response)=>{
            let orders = response.orders;
            let state = getState();
            let user = state.users.find((user)=>user.validate === true);
            let admin = state.admin.find((admin)=>admin.validate === true);
            if(state.orders === orders){
                return
            }
            if(admin !== undefined){
                dispatch(postOrders(orders))
            }
            else if (user !== undefined){
                orders.find((order)=> {
                    if(order.user === undefined){
                        return;
                    }
                    return order.user.email === user.email
                })
                dispatch(postOrders(orders))
            }
            
           
        });
    };
}

export const clearUserOrders = () => {
    console.log('HERE');
    return  (dispatch , getState) => {

        dispatch(clearOrders())   
    }
}

export const addOrder =
    ({ id ,serviceType = '', note = '',quantityType='', amount = 0,deliveryType='' ,createdAt = 0 ,lat , lng  , user , status = "pending"} = {}) =>
    ({
        type: 'ADD_ORDER',
        order: {
            id,
            serviceType,
            quantityType,
            note,
            amount,
            deliveryType    ,
            createdAt,
            lat,
            lng,
            user,
            status
        }
    });


export const removeOrder =
    ({ id } = {}) =>
    ({
        type: 'REMOVE_ORDER',
        order: { id }
    });

export const editOrder =
    (id, updates) =>
    { console.log('in action : updates : ', updates);
        return{
            type: 'EDIT_ORDER',
            id,
            updates}
    };
