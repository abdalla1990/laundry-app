import React from 'react';
import {connect} from 'react-redux';
import {DayPickerRangeController, DateRangePicker} from 'react-dates';
import {setTextFilter , setSortByAmount , setSortByDate , setStartDate , setEndDate} from '../../actions/filters';
class OrderListFilter extends React.Component {

    state ={
        calenderFocused: null ,
        open : false 
    }
    handleClick = ()=>{
        console.log('clicked and oprn is : ', this.state.open);
        let open = this.state.open  === false ? open  = true : open = false;
        console.log('open is ', open)
        this.setState(()=>({open}));

        console.log('clicked and oprn is : ', this.state.open);
    }
    render (){
       
        return (
            <div className="filters-container">
            
                <p className="filters-title" onClick={this.handleClick}>Apply Filters</p>
                    
                <div className="filters-box">
                    {this.state.open && <div>
                            <div className="filter-label">
                                <p className="labels">filter by text</p>
                                <input  className="text-filter-input"type="text" value= {this.props.filters.text} onChange={(e)=>{
                            
                                    this.props.dispatch(setTextFilter(e.target.value));
                            
                                }}/>
                            </div>
                            
                            <div>
                                <div>
                                    <button onClick={()=>{this.props.dispatch(setSortByDate())}}type="button" className="button">Date</button>
                                    <button onClick={()=>{this.props.dispatch(setSortByAmount())}}type="button" className="button">Amount</button>
                                </div>
                                <div style={{margin: "10px"}}>
                                    <DateRangePicker
                                    startDate={this.props.filters.startDate} // momentPropTypes.momentObj or null,
                                    endDate={this.props.filters.endDate} // momentPropTypes.momentObj or null,
                                    onDatesChange={({ startDate, endDate }) => {
                                        this.props.dispatch(setStartDate(startDate));
                                        this.props.dispatch(setEndDate(endDate));
                                    }} // PropTypes.func.isRequired,
                                    focusedInput={this.state.calenderFocused} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                    onFocusChange={calenderFocused => this.setState({ calenderFocused })} // PropTypes.func.isRequired,
                                    numberOfMonths = {1}
                                    isOutsideRange={()=>false}
                                    showClearDates= {true}
                                    />
                                </div>
                            
                            </div>
                            
                        </div>}
                
                </div>
                
            
            </div>
        )
    }
}


const MapStateToProps = (state)=>{ 
    
     return {
        filters : state.filters
     };
 }
 
 export default connect(MapStateToProps)(OrderListFilter);