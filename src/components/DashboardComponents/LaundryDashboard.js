import React from 'react';
import OrderList from './OrdersList';
import OrderListFilter from './OrderListFilter';

class LaundryDashboard extends React.Component{

   
        render(){


            return (
                <div className="orders-container">
                    <OrderListFilter />
                    <OrderList />
                </div>
            )
        }


}

export default LaundryDashboard ;