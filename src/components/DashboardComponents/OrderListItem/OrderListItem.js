import React from 'react';
import { Link } from 'react-router-dom';
import GoogleMapsComponent from '../../OrderComponents/GoogleMapComponent';
import moment from 'moment';
import OrderCard from './OrderCard';
import {  DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';
class OrderListItem extends React.Component{

    constructor(props){
        super(props);
        this.state={
            toggle : false,
            photos:[
                "https://images.unsplash.com/photo-1466837581110-aeaa58fb562f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5dd6a607d1fdbf2aa7a5b98040c20dba&auto=format&fit=crop&w=1500&q=80",
                "https://images.unsplash.com/photo-1518728821459-460f970ba2f2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=415dbfdd1863fe35918a0c2ec250d009&auto=format&fit=crop&w=1528&q=80",
                "https://images.unsplash.com/photo-1512460439543-a155b10e5df3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e52a0ce5f68d4b1f24efe391e8dba835&auto=format&fit=crop&w=668&q=80",
                "https://images.unsplash.com/photo-1517580768147-cb51c6eaa761?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=515ad35a55cb1b6ca9931241c02be9cf&auto=format&fit=crop&w=668&q=80"
            ]
        }
        // this.messagesEnd=undefined;
    }
    handleCardClick = ()=>{ 
        
        this.setState((prevState)=>({toggle: !prevState.toggle , scrolled:!prevState.scrolled}));
         console.log('I AM CLICKED ',this.state.toggle); 
         scroll.scrollToBottom();
    }
    
    render(){
        console.log(' orderListItem props : ', this.props.order);
        let order = this.props.order;
        const momentCreatedAt = moment(Number(order.createdAt)).format("MMM Do YY");
        var photo = this.state.photos[Math.floor(Math.random() * this.state.photos.length)];
        const OrderCardItem = (
            <OrderCard   
                date={momentCreatedAt} 
                quantity={order.quantityType}
                photo={photo}
                onClick={this.handleCardClick}
                className="order-item-card"
                price={order.amount/100}
                open={this.state.toggle}
                
            >
            
                <p className="order-item-card-text">Location</p>  
                
                <GoogleMapsComponent
                    isMarkerShown={true}
                    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div className="map"/>}
                    containerElement={<div className="map" style={{ width:`240px` , height: `300px` }} />}
                    mapElement={<div className="map" style={{ height: `50%` }} />}
                    lat ={order.lat}
                    lng ={order.lng}
                />
                  
                

                <Link className="order-item-card-text edit" to={`/edit/${order._id}`}>Edit</Link>
                
            </OrderCard>)
        

        const OrderConfirmation =
            (
                <div className="order-item-card">
                    {
                        <p className="order-item-card-header">{momentCreatedAt}</p>}
                    <div className="order-item-card-body">
        
                        <div className="order-item-card-text-container">
                            <p className="order-item-card-text">{order.quantityType}</p>
                            <p className="order-item-card-text">${order.amount/100}</p>
                        </div>
                        
                        <div className="order-item-card-map">
                            <GoogleMapsComponent
                                isMarkerShown
                                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                                loadingElement={<div className="map" style={{ height: `50%` }} />}
                                containerElement={<div className="map" style={{ width:`300px` , height: `300px` }} />}
                                mapElement={<div className="map" style={{ height: `50%` }} />}
                                lat ={order.lat}
                                lng ={order.lng}
                            />
                        </div> 
                        
                    </div>
               
                 </div>
            )
        


        return (
            <div className="order-item-container">
                {this.props.mode !== 'confirmation' ? OrderCardItem : OrderConfirmation}
            </div>
        )
    }
    }


 export default OrderListItem ;
 