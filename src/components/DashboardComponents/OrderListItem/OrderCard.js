import React from 'react';
import {UserCard} from 'react-ui-cards';
const OrderCard = (props)=>{
    
        return (
                <div {...props}>
                    <UserCard   
                        name={props.date} 
                        positionName={props.quantity}
                        cardClass="float" 
                        header={props.photo}
                        price={props.price}
                        description={props.description}
                    />
                    { props.open && <div className="order-card-extension">{props.children}</div>}
                </div> 
            )
        }
    
 export default OrderCard ;
 