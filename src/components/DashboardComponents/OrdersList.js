import React from 'react';
import OrderListItem from './OrderListItem/OrderListItem';
import { connect } from 'react-redux';
import getVisualorders from '../../selectors/orders';
import {loadState,saveState} from '../../localstorageApi/localstorageApi';
import getUser from '../../actions/users'
const OrdersList = (props)=>{
console.log('props in orderList : ', props)
    
    return (
        <div className="orders-container">
            <h1 className="orders-text">orders</h1>
            <div className="order-cards-container">
                {props.orders.map((order,index)=>{
                    console.log('order :', order)
                    return (
                        <OrderListItem  key={index} order={order} />
                    )
                })}
            </div>
        </div>
       
);
}
    

const MapStateToProps = (state)=>{ 
   
    let user =state.users.find((user)=>user.validate === true);
    let admin = state.admin.find((admin)=>admin.validate === true);
    console.log('orderList user and admin : ', user , admin);
    return {
        orders :  getVisualorders(state.orders,state.filters,user,admin)
    };
}

export default connect(MapStateToProps)(OrdersList);

 