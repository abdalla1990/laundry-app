import React from 'react';
import GoogleMapsComponentForOrders from './GoogleMapsComponentForOrders';
import OrderTable from '../AdminComponents/AdminDashboardSlides/OrderTable';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import axios from 'axios';
import {editOrder} from '../../actions/orders';
import {connect} from 'react-redux';
import {getAroundOrders} from '../../selectors/orders';
import Sliders from '../AssetsComponents/Sliders';
import _ from 'lodash';
import CircularProgress from 'material-ui/CircularProgress';
import {editOrderHandler ,getOrders} from '../../add-edit-order-handler/addEditOrderHandler';
import NotificationCenter from '../AssetsComponents/NotificationCenter';
import Modal,{closeStyle} from 'simple-react-modal';
class Driver extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            expandCard : false,
            orders : [],
            distance : 10000,
            disableSlider:true,
            isMarkerShown: true,
            lat : 0,
            lng : 0,
            order : undefined,
            orderStatus : undefined,
            error : undefined,
            lats:[],
            lngs:[],
            auth : undefined,
            displayModal:false,
            message:""
        }
        this.handleSliderValueChange = _.debounce(this.handleSliderValueChange,1000);
    }
    
    show(){
        this.setState({show: true})
    }
     
    close(){
        this.setState({show: false , displayModal : false , displayUserInfo : false})
    }
    componentWillMount = ()=>{
        console.log(' will mount ');
        this.filterOrders();

    }
    componentWillReceiveProps = (props)=>{
        console.log('PROPS FROM DIRVER : ',props);
    }
    filterOrders = () =>{
        // getOrders().then((res)=>{
            console.log(' filter Orders ');
            let self = this ;
            let watchID = navigator.geolocation.getCurrentPosition((position) => {
                console.log('my location : '+position.coords.latitude, position.coords.longitude);
                self.setState(()=>({lat : position.coords.latitude , lng :  position.coords.longitude}));
                console.log(' distance is : ', self.state.distance);
                
                let aroundOrders = getAroundOrders(this.props.orders , self.state.lat , self.state.lng,self.state.distance);
                console.log('around Orders ', aroundOrders);
                self.setState(()=>({
                    orders : aroundOrders ,
                    disableSlider:false
                }))
                let aroundOrdersLats = aroundOrders.map((order)=>{
                    let lat = order.lat ;
                    return {
                        lat
                    }
                });
                let aroundOrdersLngs = aroundOrders.map((order)=>{
                    let lng = order.lng ;
                    return {
                        lng
                    }
                });
                this.forceUpdate();
                console.log('around Orders lats and lngs ', aroundOrdersLats , aroundOrdersLngs);
                self.setState(()=>({lats :aroundOrdersLats,lngs :aroundOrdersLngs } ))   
                this.props.admin.map((admin)=>{
                    if(admin.auth !== undefined){
                        this.setState(()=>({auth:admin.auth}))
                        return;
                    }
                });

            });
        // })
        
    }
    handleMarkerClick = (e) => {
        console.log('mark clicked', e.latLng.lat());
        let query = `https://www.google.com/maps/search/?api=1&query=${e.latLng.lat()},${e.latLng.lng()}`
        fetch(query).then((res)=>{
            console.log(res);
            var win = window.open(res.url, '_blank');
            win.focus();
        })
    }
    handleNavigate = (e) => {
        console.log('mark clicked', this.state.lat , this.state.lng);
        let query = `https://www.google.com/maps/search/?api=1&query=${this.state.lat},${this.state.lng}`
        fetch(query).then((res)=>{
            console.log(res);
            var win = window.open(res.url, '_blank');
            win.focus();
        })
    }
    handelMapClick = (e)=>{
        if(this.state.lat){

        }else {

        let lat =  e.latLng.lat() ; 
        let lng =  e.latLng.lng() ; 
        console.log(' i am clicked ', lat +'   '+lng );
        this.setState({ lat , lng })

        }
        
    } 
    handleOrderStatus = (e)=>{
        console.log(' orders status : ', this.state.orderStatus );
        console.log(' order : ', this.state.order );
        let newStatus = this.state.order.status === "complete"? newStatus="pending":newStatus="complete"
        let newOrder = {
            _id: this.state.order._id,
            serviceType: this.state.order.serviceType,
            quantityType: this.state.order.quantityType,
            amount: this.state.order.amount,
            note: this.state.order.note,
            createdAt:this.state.order.createdAt,
            lat : this.state.lat,
            lng:this.state.lng,
            status:newStatus,
            user:this.state.order.user,
            auth:this.state.auth,
            access : 'admin'
        }
        console.log('new order : ', newOrder);
        if(newOrder.auth == undefined){
            console.warn('UNAUTHORIZED');
            this.setState(()=>({
                displayModal:true,
                show:true,
                message:'oops, you are not authorized , make sure that you are logged in as admin'
            }));
            return;
        }
        editOrderHandler(newOrder).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                this.props.dispatch(editOrder(this.state.order.id,newOrder));
                this.setState(()=>({
                    order:newOrder,
                    displayModal:true,
                    show:true,
                    message:'Order status has been updated to :' + newOrder.status
                }));
                console.log('this.state' , this.state);

                
            }else {
                console.log(res.data);

            }

            }).catch((err)=>{
                console.log('err',err);
            })
        

    }
    handleSliderValueChange = (value)=>{

        console.log('value is changing , ', value );
        this.setState(()=>({distance : value , disableSlider:true}));
        this.filterOrders(); 
 
    }
    DisplayUserInfo = ()=>{
        return (
            <div>
      
      <Modal
        style={{background: 'gray'}} //overwrites the default background 
        containerStyle={{background: 'white'}} //changes styling on the inner content area 
        containerClassName="test"
        closeOnOuterClick={true}
        show={this.state.show}
        onClose={this.close.bind(this)}>
    
        <a style={closeStyle} onClick={this.close.bind(this)}>X</a>
      <div>{this.state.order.user._id}</div>
 
      </Modal>
      </div>

        )
    }

    DisplayModal = (props)=>{
        return (
            <div>
      
                <Modal
                    style={{background: 'gray'}} //overwrites the default background 
                    containerStyle={{background: 'white'}} //changes styling on the inner content area 
                    containerClassName="test"
                    closeOnOuterClick={true}
                    show={this.state.show}
                    onClose={this.close.bind(this)}>
                
                    <a style={closeStyle} onClick={this.close.bind(this)}>X</a>
                <div>{props.message}</div>
            
                </Modal>
            </div>

        )
    }
    render(){

        console.log('propsand state  in driver : ', this.props , this.state );
        return (
            <div className="driver-container">
            
                <div className="slider" >
                    <Sliders  handleValueChange={this.handleSliderValueChange} disabled={this.state.disableSlider}/>
                </div>
                <div className="slider-circle" >
                    {this.state.disableSlider && <CircularProgress  size={80} thickness={5}/>}
                </div>
                 <GoogleMapsComponentForOrders
                    isMarkerShown = {this.state.isMarkerShown}
                    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `400px` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                    onMarkerClick={this.handleMarkerClick}
                    handelMapClick={this.handelMapClick}
                    lat ={this.state.lats || 0}
                    lng ={this.state.lngs || 0}
                />
               
                {this.state.orders && !this.state.disableSlider  ?  <OrderTable orders={this.state.orders} access={'driver'} 
                selection={({order})=>{
                    console.log(' the order selected on driver : ', order);
                    this.setState(()=>({expandCard:true}))
                    this.setState({ lat:order.lat ,
                                    lng: order.lng ,
                                    order,
                                    orderStatus : order.status
                                })
                    setTimeout(()=>window.scrollTo(0, document.body.scrollHeight),300 )          
                }}/> : <p style={{textAlign:"center"}} >No Data Found</p>}
                <Card className="order-card" expanded={this.state.expandCard}>
                {this.state.error}
                    
                   
                    <CardText expandable={true} style={{display:'flex',justifyContent:'center',flexDirection:'column' , alignItems:'center'}}>
                        <CardHeader
                        title="Order"
                        subtitle="Order Details"
                        actAsExpander={true}
                        showExpandableButton={true}
                        
                        />
                        <CardActions>
                            <FlatButton label="Navigate" onClick={this.handleNavigate}/>
                            <FlatButton label="Change Order Status"  onClick={this.handleOrderStatus}/>
                        </CardActions>
                        <div style={{display:'flex',flexDirection:'column', backgroundColor:'gray' ,width:'40%', textAlign:'center' }}>
                            <span><p>id : {this.state.order ? this.state.order._id : undefined}</p></span>
                                
                            <span><p>amount : {this.state.order ? this.state.order.amount : undefined}</p></span>
                                
                            <span><p>ServiceType : {this.state.order? this.state.order.serviceType : undefined}</p></span>
                                
                            <span><p onClick={()=>(this.setState(()=>({displayUserInfo:true,show:true})))}>User : {this.state.order ? this.state.order.user._id :undefined}</p></span>
                                
                            <span><p>status :{this.state.order?this.state.order.status :undefined }</p></span>
                            
                        </div>
                            
                    </CardText>
                </Card>
                {this.state.displayUserInfo && <this.DisplayUserInfo/>}
                {this.state.displayModal && <this.DisplayModal message={this.state.message} />}
            
            </div>
        )
    }


}

const MapStateToProps = (state)=>{ 
    
    return {
        user :  state.users,
        admin : state.admin,
        orders : state.driverOrders
        };
}

    export default connect(MapStateToProps)(Driver);