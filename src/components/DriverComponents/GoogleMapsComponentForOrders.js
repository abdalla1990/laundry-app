import React from 'react';
import {withScriptjs,withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import Markrs from './Markrs';
const GoogleMapsComponentForOrders = withGoogleMap((props) =>{
console.log('props in google maps ' , props)
    
    return (
        <GoogleMap
          defaultZoom={10}
          defaultCenter={{ lat: props.lat[0] || 45.5087 , lng:  props.lat[0] || -73.554 }}
          onClick={props.handelMapClick}
          >
          {props.isMarkerShown && <Markrs lat={props.lat} lng={props.lng} onMarkerClick={props.onMarkerClick}/>}
        </GoogleMap>
    )
  })

  export default GoogleMapsComponentForOrders ;
// <MyMapComponent isMarkerShown />// Map with a Marker
