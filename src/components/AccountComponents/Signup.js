import React from 'react';
import UserAccountForm from './UserAccountForm';
import {connect} from 'react-redux';
import {addUser} from '../../actions/users';
import {validateUser} from '../../actions/users'

import { fetchUserOrders } from '../../actions/orders';
import axios from 'axios';
const Signup = (props)=>{
    return (
        <div className="login-form-container">
            <div className="login-form">
                
                {props.location.message}
                <UserAccountForm 

                onSubmit={(submittedUser)=>{
                    console.log('user :',submittedUser);
                    submittedUser.role='user';
                    
                    axios.post('https://mighty-oasis-35468.herokuapp.com/users/create-user',submittedUser).then((res)=>{
                        console.log('response', res);
                    if(res.status === 200){
                        
                        props.dispatch(addUser(res.data.user));
                        console.log('userCredentials',{email:res.data.user.email,password:submittedUser.password},'res auth',res.auth);
                        let user = res.data.user;
                        let UserAuth =res.data.auth;
                        let userId = user._id;
                        let userCredentials ={email:user.email,password:submittedUser.password};
                        userCredentials = {...userCredentials , UserAuth}
                        props.dispatch(validateUser(userCredentials));    
                        props.dispatch(fetchUserOrders(userId))
                        props.history.push('/orders');
                        
                        
                       
                        
                    }else {
                        console.log(res);
                        props.history.push({pathname:'/newuser',message:res.data.toString()});
                        
                    }
                    
                    }).catch((err)=>{
                        console.log('err',err);
                        props.history.push({pathname:'/newuser',message:err.toString()});
                    })
                
                }}
                />
            </div>
        </div>
    )
}

export default connect()(Signup);