import React from 'react';
import {connect} from 'react-redux';
import Userform from './UserAccountForm'
import UserAccountForm from './UserAccountForm';
import {editUser} from '../../actions/users';
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

import {
    blue300,
    indigo900,
    orange200,
    deepOrange300,
    pink400,
    purple500,
  } from 'material-ui/styles/colors';
  
 
  
  
class Account extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            firstName : this.props.user.firstName || '',
            lastName : this.props.user.lastName || '',
            email : this.props.user.email || '',
            password : this.props.user.password || '',
            showEditForm :false,
            message : ''
        }
    }
    style = {margin: 5};
    
    onEditClick = (e)=>{
        e.preventDefault();
        console.log(' i am clicked');
        this.setState((previous)=>{
            return {showEditForm : !previous.showEditForm}
        })
    }
    
    componentDidUpdate = ()=>{
        if(this.state.message !== ''){
            setTimeout(()=>{
                this.setState(()=>{
                    return{
                        message : ''
                    }
                })
            },5000)
            
        }
        
    }
    render (){

console.log('props in account :' , this.props);

const editForm = ()=>{

    return (
        <div>
            <div className="error-container">{this.state.message}</div>
            <UserAccountForm 
            user={this.props.user} 
            onSubmit={(user)=>{
                console.log('user before send to editing :',user);
                console.log('user id : ', this.props.user.id);
                this.props.dispatch(editUser(this.props.user.id,user));
                console.log('user  after editing :',user);
                this.setState(()=>{
                    return{
                        firstName : user.firstName,
                        lastName : user.lastName,
                        password : user.password,
                        email : user.email,
                        message : 'profile updated successfully'
                    }
                })
                
            }}/>
        </div>
    )
}

        return (
        <div className="myAccount-container"> 
            <div className="myAccount-panel">
                <p>My Account</p>
                <List>
                    <ListItem
                    disabled={true}
                    leftAvatar={
                        <Avatar
                        icon={<FileFolder />}
                        color={orange200}
                        backgroundColor={pink400}
                        size={30}
                        style={this.style}
                        />
                    }
                    >
                    {this.state.firstName}{' '}{this.state.lastName}
                    </ListItem>
                    
                    <ListItem
                    disabled={true}
                    leftAvatar={
                        <Avatar
                        icon={<FileFolder />}
                        color={orange200}
                        backgroundColor={pink400}
                        size={30}
                        style={this.style}
                        />
                    }
                    >
                    {this.state.email}
                    </ListItem>
                </List>
                <div>
                    <button onClick={this.onEditClick}>Edit</button>
                </div>

                    {this.state.showEditForm === true && editForm()}
            </div> 
        </div>
        )
    }
}

const MapStateToProps = (state)=>{ 
   
        let user =  state.users.find((user)=>user.validate === true);
        return {user};
    
   
}
export default connect(MapStateToProps)(Account) ;

