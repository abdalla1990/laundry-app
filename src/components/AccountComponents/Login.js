import React from 'react';
import UserAccountForm from './UserAccountForm';
import {connect} from 'react-redux';
import {validateUser} from '../../actions/users'
import UserLogInForm from './UserLogInForm';
import {addUser} from '../../actions/users'
import axios from 'axios';
import { fetchUserOrders } from '../../actions/orders';
const Login = (props)=>{
    console.log('log in props',props);
    return (
        <div className="login-form-container">
        <div className="error-container">{props.location.message}</div>
            
            <UserLogInForm 

            onSubmit={(userCredentials)=>{
                axios.post('https://mighty-oasis-35468.herokuapp.com/users/login',userCredentials).then((res)=>{
                    console.log('response', res);
                if(res.status === 200){
                    console.log('userCredentials',userCredentials,'res auth',res.data.auth);
                    let UserAuth =res.data.auth;
                    let user = res.data.user;
                    let userId = res.data.user._id;

                    userCredentials = {...userCredentials , UserAuth , userId}
                    let userMatch = props.users.find((user)=>{
                        
                        return user.email === userCredentials.email
                    
                    });
                    console.log(' user match is : ', userMatch);
                    if(userMatch === undefined){
                        props.dispatch(addUser(user));
                    }
                    props.dispatch(validateUser(userCredentials));    
                    props.dispatch(fetchUserOrders(userId))
                    props.history.push('/orders');
                }else {
                    console.log(res);
                    props.history.push({pathname:'/login',message:res.data.toString()});
                    
                }
                
                }).catch((err)=>{
                    console.log('err',err);
                    props.history.push({pathname:'/login',message:err.toString()});
                })
                   
                
               
            }}
            />
        </div>
    )
}

let mapPropsToState = (state)=>{

    return {
        users : state.users
    }

}
export default connect(mapPropsToState)(Login);