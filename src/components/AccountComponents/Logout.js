import React from 'react';
import UserAccountForm from './UserAccountForm';
import {connect} from 'react-redux';
import {validateUser} from '../../actions/users'
import UserLogInForm from './UserLogInForm'
const Logout = (props)=>{
    return (
        <div>
            <p>Log out</p>
            <UserLogInForm 

            onSubmit={(userCredentials)=>{
                axios.delete('https://mighty-oasis-35468.herokuapp.com/users/logout',userCredentials).then((res)=>{
                    console.log('response', res);
                if(res.status === 200){
                    console.log('userCredentials',userCredentials,'res auth',res.headers.auth);
                    let UserAuth =res.data.auth
                    userCredentials = {...userCredentials , UserAuth}
                    props.dispatch(validateUser(userCredentials));
                    props.history.push('/');
                }else {
                    console.log(res);
                    props.history.push({pathname:'/login',message:res.data.toString()});
                    
                }
                
                }).catch((err)=>{
                    console.log('err',err);
                    props.history.push({pathname:'/login',message:err.toString()});
                })
                   
               
                
                
            }}
            />
        </div>
    )
}

export default connect()(Logout);