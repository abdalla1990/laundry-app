import React from 'react';
import {Link , NavLink} from 'react-router-dom';
import getvalidatedUser from '../../selectors/users';
import getvalidatedAdmin from '../../selectors/admin';
import { connect } from 'react-redux';
import {validateUser} from '../../actions/users'
import {validateAdmin} from '../../actions/admin';
import axios from 'axios';
import MdMenu from 'react-icons/lib/md/menu';
import MdHome from 'react-icons/lib/md/home';
import MdHelp from 'react-icons/lib/md/help';
import MdAccount from 'react-icons/lib/md/account-box';
import MdAddCircle from 'react-icons/lib/md/add-circle';
import IoIosList from 'react-icons/lib/io/ios-list';
import MdDriveEta from 'react-icons/lib/md/drive-eta';
import Admin from 'react-icons/lib/md/settings-applications';
import Login from 'react-icons/lib/md/vpn-key';
import Signup from 'react-icons/lib/md/person-add';
import Logout from 'react-icons/lib/md/power-settings-new';
import AdminLogout from 'react-icons/lib/md/power-settings-new';
import Dashboard from 'react-icons/lib/md/dashboard';
import {clearUserOrders} from '../../actions/orders';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
// navLink is a library of react-router and it gives the ability to navigate through the app without refreshing or returning http requests to the server 
class Header extends React.Component{
constructor(props){
    super(props);
    console.log('props in header : ', props);
    this.state = {
        toggle: false
    }
}

componentDidMount = ()=>{
    if(this.props.user.length !==0 && this.props.admin.length !==0 ){
        console.log(' i am ckicking out the user')
        this.props.dispatch(validateUser(this.props.user));
    }
}
componentDidUpdate = ()=>{
    if(this.props.user.length !==0 && this.props.admin.length !==0 ){
        console.log(' i am ckicking out the user')
        this.props.dispatch(validateUser(this.props.user));
    }
}
render (){
    return(
        <div>
            <div className="menu-icon">
                <MdMenu style={{color : this.state.toggle ? 'white' : 'black'}} onClick={()=>{console.log('I AM CLICKED'); this.setState((prevState)=>({toggle:!prevState.toggle}))}}/>
            </div>
                <ReactCSSTransitionGroup
                    transitionName="example"
                    transitionEnterTimeout={200}
                    transitionLeaveTimeout={400}
                >
                {this.state.toggle &&   
                    <header style={{height:'100%'}}>
                        <div className="header-container">
                            <div className="menu">
                            
                                <NavLink  tooltip="Home"  className="menu-item" to="/" exact={true} > <MdHome/> </NavLink>

                                <NavLink  tooltip="Help"  className="menu-item" to="/help" exact={true}>  <MdHelp/>  </NavLink>

                                {this.props.user.length !== 0 && <NavLink  tooltip="New Order"  className="menu-item" to="/create">  <MdAddCircle />  </NavLink>} 

                                {this.props.user.length !== 0 && <NavLink  tooltip="Orders"  className="menu-item" to="/orders">  <IoIosList />  </NavLink>} 

                                {this.props.user.length !== 0 && <NavLink   tooltip="My Account" className="menu-item" to="/MyAccount">  <MdAccount/>  </NavLink>} 

                                {this.props.user.length === 0 && this.props.admin.length === 0 && <NavLink  tooltip="SignUp"  className="menu-item" to="/newuser" exact={true}> <Signup /> </NavLink>}

                                {this.props.user.length !== 0 && 
                                    <NavLink  tooltip="Log Out"  to="/" className="menu-item" 
                                        onClick={()=>{
                                            console.log(' i am calling delete with : ', this.props.user[0].auth);
                                            axios.delete('https://mighty-oasis-35468.herokuapp.com/users/logout',{headers: {'x-auth': this.props.user[0].auth}}).then((res)=>{
                                            
                                            if(res.status === 200){
                                                console.log('res after delete ',res);
                                                this.props.dispatch(clearUserOrders());
                                                this.props.dispatch(validateUser(this.props.user));
                                                
                                            }else {
                                                console.log(res);
                                                
                                                
                                            }
                                            
                                            }).catch((err)=>{
                                                console.log('err',err);
                                            
                                            })
                                            
                                            
                                        }}> <Logout /> 
                                    </NavLink>}
                                
                                {this.props.admin.length ===0 && this.props.user.length ===0 &&<NavLink  tooltip="Login"  className="menu-item" to="/login" exact={true}> <Login /> </NavLink>}

                                {(this.props.admin.length !==0 || this.props.user.role === 'admin')&& this.props.user.length ===0 &&<NavLink  tooltip="Dashboard"  className="menu-item" to="/admin/AdminDashboard" exact={true}> <Dashboard /> </NavLink>}

                                {this.props.admin.length === 0 ?
                                    <NavLink   tooltip="Admin" className="menu-item" to="/admin" exact={true}> <Admin /> </NavLink> 
                                    :<NavLink 
                                    tooltip="Log Out" 
                                        onClick={()=>{
                                            console.log(' i am calling delete with : ', this.props.admin[0].auth);
                                            axios.delete('https://mighty-oasis-35468.herokuapp.com/admin/logout',{headers: {'x-auth': this.props.admin[0].auth}}).then((res)=>{
                                            
                                            if(res.status === 200){
                                                console.log('res after delete ',res);
                                                console.log(this.props);
                                                this.props.dispatch(clearUserOrders());
                                                this.props.dispatch(validateAdmin(this.props.admin));
                                                this.props.dispatch(validateUser(this.props.admin));
                                                this.props.history.push('/');
                                            }else {
                                                console.log(res);
                                                
                                                
                                            }
                                            
                                            }).catch((err)=>{
                                                console.log('err',err);
                                            })
                                            

                                        
                                        }}   
                                        className="menu-item" to="" exact={true}> <AdminLogout /> 
                                    </NavLink>}
                                    <NavLink  tooltip="Driver"  className="menu-item" to="/driver" exact={true}> <MdDriveEta /> </NavLink>

                            </div>
                        </div>
                    </header>}
                </ReactCSSTransitionGroup>
                
                
                
            
        </div>
    )
}


    
 }
    

  const MapStateToProps = (state)=>{ 
   
    
    
    return {
        user :  getvalidatedUser(state.users),
        admin : getvalidatedAdmin(state.admin)
        };
    }

    export default connect(MapStateToProps)(Header);

