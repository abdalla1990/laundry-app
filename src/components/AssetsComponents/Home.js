import React from 'react';
import { Link } from 'react-router-dom';
import Tracking from 'react-icons/lib/md/access-time';
import NewOrder from 'react-icons/lib/md/add';
import MyAccount from 'react-icons/lib/md/account-box';
import Services from 'react-icons/lib/md/local-laundry-service';
import styled, { keyframes, ThemeProvider } from 'styled-components';
const Home = (props)=>{
   const images = [
        "https://images.unsplash.com/photo-1519638617638-c589a8ba5b76?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=900aca2c3ae08f007cc162ff78c6b500&auto=format&fit=crop&w=1778&q=80",
        "https://images.unsplash.com/photo-1496603509018-385f27c3a096?ixlib=rb-0.3.5&s=da27182f28f930c144991dbac3aa26ab&auto=format&fit=crop&w=1300&q=80",
        "https://images.unsplash.com/photo-1503993319832-3d3170298339?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=fffe145ab771042b5870e154bb4874d5&auto=format&fit=crop&w=1351&q=80",
        "https://images.unsplash.com/photo-1493146165056-bb822d9422dd?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=efde80bcea57a4932aad1b12b8ea9b2d&auto=format&fit=crop&w=1350&q=80",
        "https://images.unsplash.com/photo-1458228889997-849f20121af9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6cf0d3726d4522c13a516020625a801d&auto=format&fit=crop&w=1500&q=80"
    ]
    // const pulse = (shadow) => keyframes`
    //     to {
    //         box-shadow: 0 0 8px 8px ${shadow};
    //     }`;

    // let Image = styled.div`width: 15rem;
    //         height: 5rem;
    //         border: 1px solid black;
    //         text-align: center;
    //         box-shadow: 0 0 0 0 8;
    //         background-color: 'green';
    //         animation: ${props => pulse(props.theme.shadow)} 1.25s infinite cubic-bezier(0.66, 0, 0, 1);
    //     `;
    return (
        <div className="home-page-container">
        
            <div>
            
                <div className="home-page-title">
                    <h1>Laundry</h1>
                    <h3 className="home-page-sub-title anim-typewriter" > Get your laundry done effortlessly!</h3>
                    
                </div>
                
                <div className="home-page-slider">
                    <img src={images[1]} alt="" style={{width:'100%',height:'100%'}}/>
                </div>
                <div className="home-icons-container">

                    <Link className="home-icons" to={`/orders`}><Tracking width="140" height="140" /> <p>Order Tracking</p></Link>
                    <Link className="home-icons" to={`/create`}><NewOrder width="140" height="140" /> <p>New Order</p></Link>
                    <Link className="home-icons" to={`/MyAccount`}><MyAccount width="140" height="140" /> <p>My Account</p></Link>
                    <Link className="home-icons" to={`/help`}><Services width="140" height="140"  /> <p>Services</p></Link>
                        
                </div>
                
            </div>

            
        
        </div>
    )
    
}



 
 export default Home ;