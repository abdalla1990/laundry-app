import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { css } from 'glamor';
  class NotificationCenter extends Component {

    constructor(props){

      super(props);
      this.state = {
        message : this.props.message, 
        time : this.props.time,
        type: this.props.type
      }
      
    }
    
    
    componentWillMount = () => {

      this.state.type === 'notification' && 
      toast.success(this.state.message ,{
        position: toast.POSITION.TOP_CENTER,
        className: css({
          fontSize: "40px"
        })
      });
      
      
      this.state.type === 'error' && 
      toast.error(this.state.message, {
        position: toast.POSITION.TOP_LEFT,
        className: css({
          fontSize: "40px"
        })
      });
      
      
      this.state.type === 'warn' && 
      toast.warn(this.state.message, {
        position: toast.POSITION.BOTTOM_LEFT
      });
      this.state.type === 'info' && 
      toast.info(this.state.message, {
        position: toast.POSITION.BOTTOM_CENTER
      });

    }

    render(){
      return (
        <div>
          <ToastContainer autoClose={this.state.time}/>
        </div>
      );
    }
  }

  export default NotificationCenter ;