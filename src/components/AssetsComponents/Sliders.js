import React, {Component} from 'react';
import Slider from 'material-ui/Slider';

/**
 * The slider bar can have a set minimum and maximum, and the value can be
 * obtained through the value parameter fired on an onChange event.
 */
export default class SliderControlled extends Component {
  constructor(props){
      super(props);
      this.state = {
        secondSlider: 10000,
      };
  }
    

  handleSecondSlider = (event, value) => {
    this.setState({secondSlider: value});
    this.props.handleValueChange(value);
  };

  render() {
    return (
      <div>
        <p style={{textAlign:"center" , color:"white"}}>
          <span>{'Display orders within: '}</span>
          <span>{this.state.secondSlider} Meters</span>
        </p>
        <Slider
          className="slider"
          min={0}
          max={10000}
          step={1}
          value={this.state.secondSlider}
          onChange={this.handleSecondSlider}
          disabled={this.props.disabled}
        />
        
      </div>
    );
  }
}
