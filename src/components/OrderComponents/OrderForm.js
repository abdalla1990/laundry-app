import React from 'react';
import ReactDOM from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import uuid from 'uuid'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import GoogleMapsComponent from '../OrderComponents/GoogleMapComponent'
import moment from 'moment';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import {  DirectLink, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';
class OrderForm extends React.Component {

    constructor(props){
        super(props); 
         this.state = {
            _id : props.order? props.order._id :'',
            note: props.order ? props.order.note :'',
            amount :props.order ? (props.order.amount/100).toString() : 0,
            createdAt : props.order ? moment(Number(props.order.createdAt)) : moment(),
            calenderFocused : false,
            error : '',
            isMarkerShown: true,
            lat:props.order? props.order.lat :0,
            lng:props.order? props.order.lng :0,
            serviceType:props.order?props.order.serviceType : [],
            quantityType : props.order? props.order.quantityType : '',
            status : props.order? props.order.status : '',
            user : props.order?props.order.user : {},
            scrollPosition : 0
         }  
         this.services = [
            'Clothes',
            'Blankets',
            'Curtains',
            'Shoes',
            'Carpets'
          ];
          this.quantities = [
            '1/2 KG',
            '1 KG',
            '1.5 KG',
            '2 KG',
            'More ..'
          ]

    }
    componentDidUpdate = ()=>{
        scroll.scrollToBottom();
        
    }
    componentWillMount = ()=>{
        // scroll.scrollToBottom();
        console.log(' i am in will mount ############')
        if(this.props.state !== 'remove' && this.props.state !== 'edit' && this.state.lat === 0 && this.state.lng === 0){
            console.log(' i am inside !!! #############')
            let watchID = navigator.geolocation.getCurrentPosition((position) => {
                console.log('my location : '+position.coords.latitude, position.coords.longitude);
                this.setState(()=>({lat : position.coords.latitude , lng :  position.coords.longitude}));
                this.renderMap();
            });
        }
        this.renderMap();
    }
    componentWillUnmount = ()=>{
        
        this.setState(()=>{
           return {
            _id : '',
            description :  '',
            note: '',
            amount : 0,
            createdAt : moment(),
            calenderFocused : false,
            error : '',
            isMarkerShown: true,
            lat:0,
            lng :0,
           } 
        })
        
        
        
    }
    onReset = ()=>{
       
        this.setState(()=>{
            return {
            serviceType : this.props.order ? this.props.order.serviceType : [],
            quantityType : this.props.order? this.props.order.quantityType : '',
            note: this.props.order ? this.props.order.note :'',
            amount :this.props.order ? (this.props.order.amount/100).toString() : 0,
            createdAt : this.props.order ? moment(Number(props.order.createdAt)) : moment(),
            calenderFocused : false,
            error : '',
            isMarkerShown: true,
            lat:this.props.order? this.props.order.lat :this.state.lat,
            lng:this.props.order? this.props.order.lng :this.state.lng,
            status : this.props.order? this.props.order.status : '',
            user : this.props.order?this.props.order.user : {}
            }
        })
    }

    handleformSubmittion = (e)=>{
        
        e.preventDefault();

        if(this.state.serviceType.length<1 || !this.state.quantityType){
            console.log(this.state.serviceType , this.state.quantityType);
            this.setState(()=>({error:'service or quentity fields can\'t be empty'}))
        }else if(!this.state.lat || !this.state.lng) {
            this.setState(()=>({error:'choose a location'}))
        }
        else{
            //console.log(' i am in else (order form the ide is : ',this.state.id , 'and in the props it is : ', this.props.order.id);
            this.props.onSubmit({
                _id:this.state._id,
                serviceType : this.state.serviceType,
                quantityType : this.state.quantityType,
                note:this.state.note,
                amount:parseFloat(this.state.amount , 10)* 100,
                createdAt:this.state.createdAt.valueOf(),
                lat:this.state.lat,
                lng:this.state.lng,
                status:this.state.status,
                email:this.state.user.email,
                password:this.state.user.password,
                user:this.state.user
            });

        }
      

    }

    handleChange = (event, index, serviceType) => this.setState({serviceType});
    handleQuantityChange = (event, index, quantityType) => this.setState({quantityType});
    noteOnChange =  (e)=>{
        const note = e.target.value ; 
        this.setState(()=>({note}));
    }
    amountOnChange =  (e)=>{
        const amount = e.target.value ; 
        if(!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)){
            this.setState(()=>({error : '',amount}));
        }
        
    }
    onDateChange = (createdAt)=>{
        if(createdAt)
        {
            this.setState(()=>({createdAt})) // to prevent the browser from allowing the user to delete the date manually
        }
        
    }
    onFocusChange = ({focused})=>{
        this.setState(()=>({calenderFocused:focused}))
    }

    delayedShowMarker = () => {
        setTimeout(() => {
          this.setState({ isMarkerShown: true })
        }, 3000)
      }
    handleMarkerClick = () => {
        this.setState({ isMarkerShown: false });
        this.delayedShowMarker();

      }
    handelMapClick = (e)=>{
    let lat =  e.latLng.lat() ; 
    let lng =  e.latLng.lng() ; 
    this.setState(()=>({scrollPosition:window.scrollY}));
    console.log(' i am clicked ', lat +'   '+lng );
    this.setState({ lat , lng })
    this.renderMap();
    } 
       
    menuItemsForSerivce(serviceType) {
    return this.services.map((service) => (
        <MenuItem
        key={service}
        insetChildren={true}
        checked={this.state.serviceType && this.state.serviceType.indexOf(service) > -1}
        value={service}
        primaryText={service}
        />
    ));
    }
    menuItemsForQuantity(quantityType) {
    return this.quantities.map((quantity) => (
        <MenuItem
        key={quantity}
        insetChildren={true}
        checked={this.state.quantityType ? true : false}
        value={quantity}
        primaryText={quantity}
        />
    ));
    }
     

    renderMap = ()=>{
        // this.timeOut = setTimeout(()=>this.setState(()=>({map:null})))
        this.timeOut = setTimeout(()=>this.setState(()=>({

            map : 
            <GoogleMapsComponent
            
                isMarkerShown = {this.state.isMarkerShown}
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `400px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
                onMarkerClick={this.handleMarkerClick}
                handelMapClick={this.handelMapClick}
                lat ={this.state.lat}
                lng ={this.state.lng}
            />
        })))

    }
    render (){ 
        
        console.log('location form order form  : ', this);
        console.log('order form props : ', this.props);
        console.log('order form state : ', this.state);
       return (
            <div className="order-form-container">
            {this.state.error}
            
              <form className="order-form-form" onSubmit={this.handleformSubmittion}>
                <div className="fields--date">
                    <div className="order-form-fields">
                        <SelectField
                            multiple={true}
                            hintText="Service Type"
                            value={this.state.serviceType}
                            onChange={this.handleChange}
                            
                        >
                        {this.menuItemsForSerivce(this.state.serviceType)}
                        </SelectField>
                        <SelectField
                            multiple={false}
                            hintText="Quantity"
                            value={this.state.quantityType}
                            onChange={this.handleQuantityChange}
                        >
                        {this.menuItemsForQuantity(this.state.serviceType)}
                        </SelectField>
                        <TextField
                        hintText="Amount"
                        fullWidth={true}
                        value={this.state.amount===0?'':this.state.amount}
                        onChange={this.amountOnChange}
                        />
                        <TextField
                        hintText="Note"
                        fullWidth={true}
                        value={this.state.note}
                        onChange={this.noteOnChange}
                        />
                    </div> 
                    <div className="order-form-date">
                        <h3 style={{textAlign : "center"}}>Delivery Date</h3>
                        <SingleDatePicker 
                        date = {this.state.createdAt}
                        onDateChange = {this.onDateChange}
                        focused={this.state.calenderFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={()=> false}
                        />
                    </div>
                </div>
                <div className="order-form-map">
                    
                    {this.state.map}
                    
                </div>
                    
                    <div className="col-sm-12" style={{textAlign : "center"}}>
                        <button className="button-primary small" type="submit">{this.state._id?'edit order':'add order'}</button>
                        {this.props.state !== 'remove' &&<button  className="button-primary small" onClick={this.onReset}>Reset</button>}
                    </div>
                
              </form>
              
              
            </div>
          );
    }
} 

export default OrderForm ;
