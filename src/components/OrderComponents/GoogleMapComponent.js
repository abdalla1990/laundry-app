import React from 'react';
import {withScriptjs,withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import {GoogleApiWrapper} from 'react-google-maps'
const GoogleMapsComponent = withGoogleMap((props) =>{

    
    return (
        <GoogleMap
          // googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyApy18Fm2Ojp6_9nnKzL0-BSzzwPr0cb1o"
          defaultZoom={15}
          defaultCenter={{ lat: props.lat === 0 ? 45.5087 : props.lat, lng: props.lng === 0 ? -73.554 : props.lng }}
          onClick={props.handelMapClick}
          >
          {props.isMarkerShown && <Marker position={{ lat: props.lat, lng: props.lng }} onClick={props.onMarkerClick}/>}
      </GoogleMap>
    )
  })

  export default GoogleMapsComponent ;
// <MyMapComponent isMarkerShown />// Map with a Marker
