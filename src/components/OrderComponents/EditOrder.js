import React from 'react';
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import {removeOrder,editOrder,fetchUserOrders} from '../../actions/orders';
import getvisiableorders from '../../selectors/orders';
import OrderForm from './OrderForm';
import axios from 'axios';
import {editOrderHandler,removeOrdersHandler} from '../../add-edit-order-handler/addEditOrderHandler';
const EditOrderPage = (props)=>{

      
           
            return (
                <div className="add-order-container">
                    
                    <OrderForm 
                    {...props} 
                        order ={props.order}
                        onSubmit={(order)=>{
                                console.log('order', order);
                                

                                editOrderHandler(order).then((res)=>{
                                console.log('response', res);
                                
                                if(res.status === 200){
                                    console.log('order',order);
                                    order.auth = props.user.auth
                                    props.dispatch(editOrder(order._id,order));
                                    props.history.push('/');
                                }else {
                                    console.log(res.data);
                                    props.history.push({pathname:'/edit/'+order._id,message:res.data.toString(),order:order});

                                }

                                }).catch((err)=>{
                                    console.log('err',err);
                                    props.history.push({pathname:'/edit/'+order._id,message:err.toString(),order:order});
                                })
                            }
                    }/>
                    <button onClick={()=>{ 
                        console.log( 'deleting ' , props.order._id);

                        removeOrdersHandler(props.user.auth,props.order._id).then((res)=>{
                            console.log('response', res);
                            
                            if(res.status === 200){
                                console.log('order',props.order);
                                props.order.auth = props.user.auth;
                                props.dispatch(removeOrder({id:props.order._id}));
                                props.dispatch(fetchUserOrders());
                                props.history.push('/');
                            }else {
                                console.log(res.data);
                                props.history.push({pathname:'/edit/'+order._id,message:res.data.toString(),order:order});

                            }

                            }).catch((err)=>{
                                console.log('err',err);
                                props.history.push({pathname:'/edit/'+order._id,message:err.toString(),order:order});
                            })
                        
                    }} type="submit"  className="button-primary small" style={{width: "30px",padding: "2.5px"}}>remove order</button>
                      
                    
                    
                </div>
            )
        
           

}
const MapStateToProps = (state,props)=>{ 
    
     return {
        user :   state.users.find((user)=>user.auth !== undefined),
        order :  state.orders.find((order)=>props.match.params.id === order._id)
     };
 }

export default connect(MapStateToProps)(EditOrderPage);