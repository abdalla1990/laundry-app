import React from 'react';
import OrderListItem from '../DashboardComponents/OrderListItem/OrderListItem'
import {addOrder} from '../../actions/orders'
import {connect} from 'react-redux';
import axios from 'axios';
import uuid from 'uuid'
import {addOrderHandler} from '../../add-edit-order-handler/addEditOrderHandler';
import {fetchUserOrders} from '../../actions/orders';
import validUser from '../../selectors/users';
class OrderConformation extends React.Component{

    confirm = ()=>{
        let localOrder= this.props.location.state.order;
        localOrder.status = "pending";
        let order = {
            auth : localOrder.user.auth,
            createdAt:localOrder.createdAt,
            lat:localOrder.lat,
            lng:localOrder.lng,
            status:localOrder.status,
            note:localOrder.note,
            amount:localOrder.amount,
            serviceType:localOrder.serviceType,
            quantityType:localOrder.quantityType,
        }
        console.log('order : ', order);
        addOrderHandler(order).then((res)=>{
            console.log('response', res);
        if(res.status === 200){
            console.log('given order',res.order);
            order.id = res.order._id;
            order.user = {
                id : localOrder.user.id,
                email :localOrder.user.email,
                password:localOrder.user.password,
                auth:localOrder.user.auth
            }
            this.props.dispatch(fetchUserOrders(this.props.user.id));
            // this.props.dispatch(addOrder(order));
            this.props.history.push('/');
        }else {
            console.log(res.data);
            this.props.history.push({pathname:'/create',message:res.data.toString(),order:order});
            
        }
        
        }).catch((err)=>{
            console.log('err',err);
            this.props.history.push({pathname:'/create',message:err.toString(),order:order});
        })
           
        
       

        
    }
    delete = () =>{
        this.props.history.push({
            pathname:'/create',
            state : {order :this.props.location.state.order}
    });
    }
   render (props) {
    console.log('props' , this.props);
        return (
            <div className="order-confirmation-container">
                <div className="order-confirmation-panel">
                    <div className="confirm-order-text">
                        <h3>please confirm your order</h3>
                    </div>
                    <div className="confirm-order-list-items">
                        <div>
                            <OrderListItem 
                                order={this.props.location.state.order}
                                mode={"confirmation"}
                            />
                        </div>
                            
            
                        
                        
                        <div className="confirm-order-buttons-container">
                            <button className="button-primary small" onClick={this.confirm}>Confirm</button>
                            <button className="button-primary small" onClick={this.delete}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
            
        )
    }
    }
    const MapStateToProps = (state,props)=>{ 
    
        return {
            user :  state.users.find((user)=>user.validate === true && user.auth !== undefined)
        };
    }
    
export default connect (MapStateToProps)(OrderConformation) ;