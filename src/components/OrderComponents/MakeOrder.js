import React from 'react';
import {connect} from 'react-redux';
import {addOrder} from '../../actions/orders';
import OrderForm from './OrderForm';
import getUser from '../../selectors/users';
class AddOrderPage extends React.Component{
    
    
    render ()
    { 
        console.log('make order props : ', this.props);
       return (

        <div className="add-order-container">

        <div className="add-order-text"><h1>New Order</h1></div>
            
            {this.props.location !== undefined && this.props.location.message}
                <OrderForm  
                    order = {this.props.location ? this.props.location.state.order :undefined}
                    onSubmit={(order)=>{
                            
                        console.log(order);
                        this.props.user ? order.user=this.props.user: undefined;
                        console.log('prooop',this.props)
                        this.props.history.push({
                            pathname:'/OrderConfirmation',
                            state : {order}
                    });
                    }}
                />
        </div>


       ) 
    
    }

}


const MapStateToProps = (state,props)=>{ 
    
    return {
        user :  state.users.find((user)=>user.validate === true)
    };
}

 
 export default connect(MapStateToProps)(AddOrderPage);

