import React, {Component} from 'react';
import { Link } from 'react-router-dom';
const AdminNavComponent = (props)=> {

    
    const MenuItem = ()=>{ 

        
        return (
            <div  className="admin-nav-menu-component">
                {props.items.map((item , index)=>{
                    return (
                        <Link key = {index} to={item.value}>{item.label}</Link>
                    )
                })}
            </div> 
        )
            
        
    };
    
    return(<MenuItem />)
    
}

export default AdminNavComponent;