import React, {Component} from 'react';
import {connect}from 'react-redux';

import AdminNavMenuComponent from './AdminNavMenuComponent';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {toggleMenu,unToggleItem} from '../../../actions/menu';
class AdminNavComponent extends Component{


    constructor(props){
        super(props);
        this.state = {
           
            relatedItems : []
        }
    }
    handleNavExpand = (givenItem)=>{
        
        let relatedItems =[];
        
        this.props.items.map((item,index)=>{
            
            if(item.ref == givenItem.label){
                
                relatedItems.push(item);

            }

            if(item.id !== givenItem.id){
            
                this.props.dispatch(unToggleItem(item.id));

            }
            
        })

        this.setState(()=>({relatedItems}));
        
        this.props.dispatch(toggleMenu(givenItem.id));
       
        
        
        
    }
    
    node = ()=>(<p className="admin-nav-component" onClick={()=>{this.handleNavExpand(this.props.item)}}>{this.props.item.label}</p>)
    
    render(){
        if(this.props.item.divider){
            return(
                <div> 
                    <this.node /> 
                        <ReactCSSTransitionGroup
                            transitionName="example"
                            transitionEnterTimeout={200}
                            transitionLeaveTimeout={400}
                        >
                        {this.props.toggled &&<AdminNavMenuComponent items={this.state.relatedItems}/>}
                        </ReactCSSTransitionGroup>
                </div>
            )
        }else{
            return null
        }
    }
    
    
}

let mapStateToPRops = (state)=>{
    return {
        menuState : state.menu
    }
}

export default connect(mapStateToPRops)(AdminNavComponent);