import React from 'react';
import Toggle from 'material-ui/Toggle';
import changerole, { changeRole } from '../../../actions/users';
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import {addAdmin,removeAdmin} from '../../../actions/admin';
import{addAdminHandler,findAdminHandler,deleteAdminHandler} from '../../../add-edit-admin-handler/addEditAdminHandler';
import {editUserHandler} from '../../../add-edit-user-handler/addEditUserHandler';
import privilageManager from './privilageManager/privilageManager';
class Privilage extends React.Component {
  constructor(props){
    super(props);
    this.roleManager;
    this.state = {
      driver : undefined, 
      admin  : undefined ,
      message : ''
    }
    this.styles= {
      block: {
        maxWidth: 250,
      },
      toggle: {
        marginBottom: 16,
      },
      thumbOff: {
        backgroundColor: '#ffcccc',
      },
      trackOff: {
        backgroundColor: '#ff9d9d',
      },
      thumbSwitched: {
        backgroundColor: 'red',
      },
      trackSwitched: {
        backgroundColor: '#ff9d9d',
      },
      labelStyle: {
        color: 'red',
      },
    };
    
  }
  
  
  toggleChange = (toggle)=>{
    
    if(toggle.target){
      this.changeToggleValues(toggle.target.id)
    }

  }

  handleSaveClick = ()=>{

    let role = this.roleManager.getUserRole();
    this.roleManager.updateRoleToDB(this.props.user,role,this.props.dispatch,this.state.driver,this.state.admin).then((res)=>{
     
      let process = res;
     
      if(process !== undefined){
        this.setState(()=>({message : process}));
      }
    })
    
  
  }


  changeToggleValues = (key)=>{
    this.roleManager.setUserRole(key);
    this.setState(()=>(
      {
        driver : this.roleManager.checkUserRole('driver'), 
        admin  : this.roleManager.checkUserRole('admin')}
      )
    )
  }


  PrivilageToggles = () => (
    <div className="privilage-toggles">
    <h3>{this.state.message}</h3>
    <h2>Choose User's Previlage</h2>
      <Toggle
      label="Driver"
      id='driver'
      defaultToggled={this.state.driver}
      onToggle={this.toggleChange}
      style={this.styles.toggle}
    />
      <Toggle
      label="Admin"
      id='admin'
      defaultToggled={this.state.admin}
      onToggle={this.toggleChange}
      style={this.styles.toggle}
    />

      <button  className="button-primary"   onClick={this.handleSaveClick}>Save Changes</button>
    </div>
  );

  componentDidMount(){
    this.roleManager = new privilageManager(this.props.user);
    this.setState(()=>{
      return {
        driver : this.roleManager.checkUserRole('driver'),
        admin : this.roleManager.checkUserRole('admin')
      }
    })
  }
  render (){
   
    return (
      <div className="privilages-container">
        <this.PrivilageToggles />
      </div>
    )
  }


}




export default connect()(Privilage) ;
