import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import uuid from 'uuid'
import {SingleDatePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import GoogleMapsComponent from '../../OrderComponents/GoogleMapComponent'
import moment from 'moment';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
class OrderForm extends React.Component {

    constructor(props){
        super(props);
        
         this.state = {
            _id : props.order? props.order._id :'',
            serviceType:props.order?props.order.serviceType : [],
            quantityType : props.order? props.order.quantityType : '',
            note: props.order ? props.order.note :'',
            amount :props.order ? (props.order.amount/100).toString() : 0,
            createdAt : props.order ? moment(Number(props.order.createdAt)) : moment(),
            calenderFocused : false,
            error : '',
            isMarkerShown: true,
            lat:props.order? props.order.lat :0,
            lng:props.order? props.order.lng :0,
            status:props.order? props.order.status :'',
            user:props.order? props.order.user :'',
         }  
         this.momentCreatedAt = moment(Number(props.order.createdAt)).format("MMM Do YY");
         this.services = [
            'Clothes',
            'Blankets',
            'Curtains',
            'Shoes',
            'Carpets'
          ];
          this.quantities = [
            '1/2 KG',
            '1 KG',
            '1.5 KG',
            '2 KG',
            'More ..'
          ]

    }
    componentWillUnmount = ()=>{
        this.setState(()=>{
           return {
            _id : '',
            serviceType :  [],
            quantityType : '',
            note: '',
            amount : 0,
            createdAt : moment(),
            calenderFocused : false,
            error : '',
            isMarkerShown: true,
            lat:0,
            lng :0,
            user:undefined
           } 
        })
        this.props.onUnMount();
    }
    onReset = ()=>{
       
        this.setState(()=>{
            return {
                serviceType : this.props.order ? this.props.order.serviceType : [],
                quantityType : this.props.order? this.props.order.quantityType : '',
                note: this.props.order ? this.props.order.note :'',
                amount :this.props.order ? (this.props.order.amount/100).toString() : 0,
                createdAt : this.props.order ? moment(Number(this.props.order.createdAt)) : moment(),
                calenderFocused : false,
                error : '',
                isMarkerShown: true,
                lat:this.props.order? this.props.order.lat :0,
                lng:this.props.order? this.props.order.lng :0,
                user:this.props.order? this.props.order.user :'',
            }
        })
    }

    handleformSubmittion = (e)=>{
        
        e.preventDefault();

        if(this.state.serviceType.length<1 || !this.state.quantityType){
            console.log(this.state.serviceType , this.state.quantityType);
            this.setState(()=>({error:'service or quentity fields can\'t be empty'}))
        }else if(!this.state.lat || !this.state.lng) {
            this.setState(()=>({error:'choose a location'}))
        }
        else{
            //console.log(' i am in else (order form the ide is : ',this.state.id , 'and in the props it is : ', this.props.order.id);
            this.props.onSubmit({
                _id:this.state._id,
                note:this.state.note,
                serviceType : this.state.serviceType,
                quantityType : this.state.quantityType,
                amount:parseFloat(this.state.amount , 10)* 100,
                createdAt:this.state.createdAt.valueOf(),
                lat:this.state.lat,
                lng:this.state.lng,
                status: this.state.status,
                user: this.state.user
            });

        }
      

    }

    
    handleChange = (event, index, serviceType) => this.setState({serviceType});
    handleQuantityChange = (event, index, quantityType) => this.setState({quantityType});

    noteOnChange =  (e)=>{
        const note = e.target.value ; 
        this.setState(()=>({note}));
    }
    amountOnChange =  (e)=>{
        const amount = e.target.value ; 
        if(!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)){
            this.setState(()=>({error : '',amount}));
        }
        
    }
    onDateChange = (createdAt)=>{
        if(createdAt)
        {
            this.setState(()=>({createdAt})) // to prevent the browser from allowing the user to delete the date manually
        }
        
    }
    onFocusChange = ({focused})=>{
        this.setState(()=>({calenderFocused:focused}))
    }

    delayedShowMarker = () => {
        setTimeout(() => {
          this.setState({ isMarkerShown: true })
        }, 3000)
      }
    handleMarkerClick = () => {
        this.setState({ isMarkerShown: false })
        this.delayedShowMarker()
      }
      handelMapClick = (e)=>{
       let lat =  e.latLng.lat() ; 
       let lng =  e.latLng.lng() ; 
       console.log(' i am clicked ', lat +'   '+lng );
       this.setState({ lat , lng })
      } 

      menuItemsForSerivce(serviceType) {
        return this.services.map((service) => (
          <MenuItem
            key={service}
            insetChildren={true}
            checked={this.state.serviceType && this.state.serviceType.indexOf(service) > -1}
            value={service}
            primaryText={service}
          />
        ));
      }
      menuItemsForQuantity(quantityType) {
        return this.quantities.map((quantity) => (
          <MenuItem
            key={quantity}
            insetChildren={true}
            checked={this.state.quantityType ? true : false}
            value={quantity}
            primaryText={quantity}
          />
        ));
      }
    render (){

        console.log('order form props : ', this.props);
       return (
            <div className="order-form-container"> 
            {this.state.error}
              <form className="order-form-form" onSubmit={this.handleformSubmittion}>
              <div className="fields--date">
                <div className="order-form-fields">
                
                  <SelectField
                  multiple={true}
                  hintText="Service Type"
                  value={this.state.serviceType}
                  onChange={this.handleChange}
              >
              {this.menuItemsForSerivce(this.state.serviceType)}
              </SelectField>
              <SelectField
                  multiple={false}
                  hintText="Quantity"
                  value={this.state.quantityType}
                  onChange={this.handleQuantityChange}
              >
              {this.menuItemsForQuantity(this.state.serviceType)}
              </SelectField>
                      <TextField
                      hintText="Amount"
                      fullWidth={true}
                      value={this.state.amount}
                      onChange={this.amountOnChange}
                      />
                      <TextField
                      hintText="Note"
                      fullWidth={true}
                      value={this.state.note}
                      onChange={this.noteOnChange}
                      />
                      <div className="order-form-date">
                        <h3 style={{textAlign : "center"}}>Delivery Date</h3>
                      <SingleDatePicker 
                        date = {this.state.createdAt}
                        onDateChange = {this.onDateChange}
                        focused={this.state.calenderFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={()=> false}
                     />
                     </div>
                     </div>
                     <div className="order-form-map">
                     <GoogleMapsComponent
                        isMarkerShown = {this.state.isMarkerShown}
                        googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                        loadingElement={<div style={{ height: `100%` }} />}
                        containerElement={<div style={{ height: `400px` }} />}
                        mapElement={<div style={{ height: `100%` }} />}
                        onMarkerClick={this.handleMarkerClick}
                        handelMapClick={this.handelMapClick}
                        lat ={this.state.lat || 0}
                        lng ={this.state.lng || 0}
                     />
                    </div>
                  
                  <div className="col-sm-12" style={{textAlign : "center"}}>
                  <button className="button-primary small" type="submit">{this.props.state} Order</button>
                    {this.props.state !== 'remove' &&<button  className="button-primary small" onClick={this.onReset}>Reset</button>}
                  </div>
                  
                
                </div>
              </form>
              
              
            </div>
          );
    }
} 

export default OrderForm ;
