import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import uuid from 'uuid'

class UserForm extends React.Component {

    constructor(props){
        super(props);
        this.props.user ?
        this.state = {
            id           : this.props.user.id  ||this.props.user._id,
            error        :                        '',
            firstName    : this.props.user.firstName|| '',
            lastName     : this.props.user.lastName || '', 
            password     : this.props.user.password || '',
            password2    : this.props.user.password2|| '',
            email        : this.props.user.email    || ''
        } : 
        this.state = {
            id           : uuid.v4(),
            error        :  '',
            firstName    :  '',
            lastName     :  '', 
            password     :  '',
            password2    :  '',
            email        :  ''
        }

    }
    componentWillUnmount = ()=>{
        this.setState(()=>{
           return {
            id           : '',
            error        : '',
            firstName    : '',
            lastName     : '', 
            password     :  '',
            password2    : '',
            email        : ''
           } 
        })
        this.props.onUnMount();
    }
    onFormSubmit = (e)=>{
        console.log(' i am submiting')
        e.preventDefault();
        
        if(this.props.state !== 'remove' && this.state.password !== this.state.password2){
            console.log('dont match')
            this.setState(()=>{
                return{
                    error :'passwords dont match'
                }
            })
        }else if (!this.state.email.includes('@')){
            this.setState(()=>{
                return{
                    error :'please enter a valid email'
                }
            })

        } else {
            console.log(' submitting for sure ')
            this.props.onSubmit({
                _id : this.state.id,
                firstName:this.state.firstName,
                lastName:this.state.lastName,
                password : this.state.password,
                email : this.state.email
            })
            
            
        }
            
        
    }
    onReset = ()=>{
       
        this.setState(()=>{
            return {
                id           : this.props.id || uuid.v4(),
                error        : '',
                firstName    : '',
                lastName     : '', 
                password     : '',
                password2    : '',
                email        : ''
            }
        })
    }
    onFNchange = (e)=>{
        const fn = e.target.value ; 
        this.setState(()=>({
            error : '',
            firstName : fn
        }))
        console.log('state after first name : ', this.state);
    } 
    onLNchange=(e)=>{
        const ln = e.target.value ; 
        this.setState(()=>({
            error : '',
            lastName : ln
        }))
        console.log('state after last name : ', this.state);

    }    
    onEmailchange=(e)=>{
        const em = e.target.value ; 
        this.setState(()=>({
            error : '',
            email : em
        }))
        console.log('state after email : ', this.state);
    }
    onPasschange = (e)=>{
        const pass = e.target.value ; 
        this.setState(()=>({
            error : '',
            password : pass
        }))
        console.log('state after pass1 : ', this.state);
    }
    onPass2change = (e)=>{
        const pass2 = e.target.value ; 
        this.setState(()=>({
            error : '',
            password2 : pass2
        }))
        console.log('state after pass2 : ', this.state);
    }
    render (){
        console.log('user form props : ', this.props);
       return (
            <div className="form-container">
            {this.state.error}
              <form className="readmin-form" onSubmit={this.onFormSubmit}>
                <div className="row">
                  <div className="col-sm-12">
                      <TextField
                      hintText="Firs tName"
                      fullWidth={true}
                      value={this.state.firstName}
                      onChange={this.onFNchange}
                      />
                      <TextField
                      hintText="Last Name"
                      fullWidth={true}
                      value={this.state.lastName}
                      onChange={this.onLNchange}
                      />
                      <TextField
                      hintText="Email"
                      fullWidth={true}
                      value={this.state.email}
                      onChange={this.onEmailchange}
                      />
                      <TextField
                      hintText="Password"
                      fullWidth={true}
                      value={this.state.password}
                      onChange={this.onPasschange}
                      />
                      {this.props.state !== 'remove' &&<TextField
                      hintText="Re-Password"
                      fullWidth={true}
                      value={this.state.password2}
                      onChange={this.onPass2change}
                      />}
                  </div>
                  <div className="col-sm-12" style={{textAlign : "center"}}>
                    {this.props.state !== 'remove' &&<button className="button-primary small" onClick={this.onReset}>Reset Info</button>}
                      <button className="button-primary small"   type="submit" >{this.props.user?'Edit Info':'Sign Up'}</button>
                      
                  </div>
                </div>
              </form>
              
              
            </div>
          );
    }
} 

export default UserForm ;
