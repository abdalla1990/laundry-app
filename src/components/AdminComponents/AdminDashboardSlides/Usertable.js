/*
 * filename: table.js
 * Get the user data from store and show
 * */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import Modeedit from 'material-ui/svg-icons/editor/mode-edit';
import ModeDelete from 'react-icons/lib/md/delete-forever';
import ModeSettings from 'react-icons/lib/md/settings-applications'
import {getUsersHandler} from '../../../add-edit-admin-handler/addEditAdminHandler'
/**
 * A simple table demonstrating the hierarchy of the `Table` component and its sub-components.
 */
class Usertable extends Component {

  constructor(props){
    super(props);
    this.state = {
      disabled : undefined,
      disableTable : true
    }
    
  }

  componentWillMount = ()=>{
    let auth = undefined;
    this.props.admin.map((admin)=>{

      if(admin.validate === true){
        auth = admin.auth;
      }
    })
    console.log('admin auth === ', auth);
    
    getUsersHandler(auth).then((users)=>{
      console.log(' Users in the system = ', users.data);
      this.setState(()=>({users:users.data , disableTable:false}));
      console.log('users in state of usertable : ', this.state.users);
      let record = []
      this.state.users.map((user,index)=>{
        record.push({index : index , id : user._id, disabled : true });
      })

      console.log('record Array in DidMount: ', record);
      this.setState(()=>{
        return {
          disabled : record
        }
      })
      console.log('disabled Array in DidMount: ', this.state.disabled);
    
    }).catch((err)=>{

      console.log('ERRRROR : ',err);
    })
  }
  handelUserPrivileges = (user)=>{
    console.log(' handelUserPrivileges',user);
    this.props.push('#privilage_users',user,'edit');
  }
  handelRemove = (user)=>{
    // console.log(' i should edit ',user , "and props r : ", this.props);
    // should warn the user first ! 
     this.props.push('#add_users',user,'remove');
   }

  handelEdit = (user)=>{
   // console.log(' i should edit ',user , "and props r : ", this.props);
    this.props.push('#add_users',user,'edit');
  }
  onRowSelection = (selectedRow)=>{
    console.log('i am selected : ', selectedRow);
    let record = [];
    this.state.disabled.map((row,index)=>{
    console.log('i am in round number ', index);
      if(row.index === selectedRow[0]){
        console.log('i am inside if')
        record.push({index : row.index , id : this.state.users[row.index].id, disabled : false })
      }else {
        console.log('i am inside else')
        record.push({index : row.index , id : this.state.users[row.index].id, disabled : true })
      }

    })
    console.log('record Array in selection: ', record);
    this.setState(()=>({disabled:record}))
    console.log('disabled Array in Selection: ', this.state.disabled);
    
  }
  showTableData () {
    return this.state.users.map((user,index) => {
      return (
        <TableRow key={user.id}>
          <TableRowColumn>{user.firstName}</TableRowColumn>
          <TableRowColumn>{user.lastName}</TableRowColumn>
          <TableRowColumn>{user.email}</TableRowColumn>
          <TableRowColumn style={{paddingLeft:"0px"}}>
          
          <div className="col-sm-12">
            <FlatButton  disabled={this.state.disabled !== undefined && this.state.disabled[index].disabled} onClick={()=>{this.handelEdit(user)}}icon={<Modeedit style={{padding: "1px"}}/>} />
            <FlatButton  disabled={this.state.disabled !== undefined &&this.state.disabled[index].disabled} onClick={()=>{this.handelRemove(user)}}icon={<ModeDelete style={{padding: "1px" , fontSize: '20px'}}/>} />
            <FlatButton  disabled={this.state.disabled !== undefined &&this.state.disabled[index].disabled} onClick={()=>{this.handelUserPrivileges(user)}}icon={<ModeSettings style={{padding: "1px",fontSize: '20px'}}/>} />
          </div>
          
          
          </TableRowColumn>
        </TableRow>
      );
    });
  }

  render() {
    
    return (
      <Table 
      onRowSelection={this.onRowSelection}
      >
        <TableHeader>
        
          <TableRow>
            <TableHeaderColumn>First Name</TableHeaderColumn>
            <TableHeaderColumn>LastName</TableHeaderColumn>
            <TableHeaderColumn>Email</TableHeaderColumn>
            <TableHeaderColumn>Action</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
        {this.state.disableTable ? <CircularProgress  size={80} thickness={5}/> :this.state.users !== undefined && this.showTableData()}
        </TableBody>
      </Table>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.users
  }
}

export default connect(mapStateToProps)(Usertable);
