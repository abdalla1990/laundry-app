import {addAdmin,removeAdmin} from '../../../../actions/admin';
import{addAdminHandler,findAdminHandler,deleteAdminHandler} from '../../../../add-edit-admin-handler/addEditAdminHandler';
import {editUserHandler} from '../../../../add-edit-user-handler/addEditUserHandler';
import { changeRole } from '../../../../actions/users';

export default (user,role,dispatch,driver,admin)=>{

    let checkIfAdminThenAdd = ()=>{

        let p = new Promise((resolve,reject)=>{
            findAdminHandler(user.email).then((res)=>{
                if(res.admin){
                    console.log('user is already an admin');
                    resolve('user is already an admin') ;
                }else{
                    console.log('admin has not been found . lets add him !  ',user);
                    
                    
                       
                    makeUserAnAdmin().then((res)=>{
                        
                        resolve(res);

                    }).catch((err)=>{

                        reject(err);

                    })
                 
                   
                }
            }).catch((err)=>{
                console.log('res', err);
                reject(err);
            })
        })

        return p; 
        
    }

    let makeUserAnAdmin = ()=>{
        
        let p = new Promise((resolve,reject)=>{
           
            addAdminHandler(user).then((res)=>{
           
                if(res.status === 200){  
                    changeUserRole('admin');
                    // I SHOULD EDIT THE EXISTING USER IN THE DATABASE TO CHANGE THE ROLE FROM USER TO ADMIN 
                    // UPDATE_USER_ROUTE IN API HAS TO BE MODIFIED TO ACCOMMUDATE THE USER PAYLOAD
                    if(user.role === 'admin'){
                       
                        dispatch(addAdmin(
                            {id:user.id,
                            firstName : user.firstName,
                            lastName:user.lastName,
                            email:user.email,
                            password:user.password,
                            password2:user.password2
                            }
                        ));
    
                        resolve(`${user.email} has become an admin`); 
                    }
    
                }else {
                    
                    reject(res.data);
                }  
    
            }).catch((err)=>{
                
                reject(err);
            })
        })

        return p ;
        
    }


    let checkIfAdminThenRemove = (role)=>{

        // find out if the user was admin , delete him from the admin table  
        // FIND OUT WHERE THE ADMIN ID IS PAPOLATED FROM , 
        // BECAUSE THE ID IN DB IS DIFFERENT FROM THE ONE IN REDUX 
  
        let p = new Promise((resolve,reject)=>{
            
            findAdminHandler(user.email).then((res)=>{
                console.log('################# res.admin', res);
                if(res.admin){
                    console.log('user is an admin turn him into a driver');
                    revertAdmin();
                
                }
                    
                    changeUserRole(role).then((res)=>{
                        
                        resolve(res);

                    }).catch((err)=>{

                        reject(err);

                    });  
            });
        })

        return p ;
        
    }


    let revertAdmin = ()=>{

        let p = new Promise((resolve,reject)=>{
            
            deleteAdminHandler(user.email).then((res)=>{
              
                dispatch(removeAdmin({
                    email:user.email
                }));
                
                dispatch(changeRole({
                    email  : user.email,
                    driver ,
                    admin
               
                }));

                resolve(res.message) ;
           
            }).catch((err)=>{
               
                reject(err);

            });
        });

        return p ;
        
    }

    let changeUserRole = (role)=>{

        let p = new Promise((resolve,reject)=>{
           
            user.role = role;
           
            editUserHandler(user).then((res)=>{
                
                if(res.status === 200){
                   
                    dispatch(changeRole({
                    email : user.email,
                    driver ,
                    admin
                    }));
    
                    resolve(`${user.email} has become ${role}`) ;

                }else{

                    reject(res.data);

                }
            });
        })

        return p ;
       
    }



    if(role == 'admin'){
        console.log('the role is : admin');
        //check if the database has this user as an admin previously. 
        return checkIfAdminThenAdd()

       

    }else if (role === 'driver'){
        console.log('thie role is  driver');
        // find out if the user was admin , delete him from the admin table  
        // FIND OUT WHERE THE ADMIN ID IS PAPOLATED FROM , 
        // BECAUSE THE ID IN DB IS DIFFERENT FROM THE ONE IN REDUX 
        return checkIfAdminThenRemove('driver');

      

      }else {
      console.log('thie role is  user');
        // find out if the user was admin , delete him from the admin table  
        // FIND OUT WHERE THE ADMIN ID IS PAPOLATED FROM , 
        // BECAUSE THE ID IN DB IS DIFFERENT FROM THE ONE IN REDUX 
        return checkIfAdminThenRemove('user');

        
      }



    
}