import privilageService from './privilageService';
class privilageManager{
    constructor(user){
        
        let userRole = user.role ;
        this.driver;
        this.admin;
        
        if(userRole === "user"){
            this.driver = false;
            this.admin = false;
        }
        
        if(userRole === "driver"){
            this.driver = true;
            this.admin = false;
        }

        if(userRole === 'admin'){
            this.driver = false;
            this.admin = true;
        }
 
    }

    checkUserRole = (role)=>{
        if(role === "admin"){
            return this.admin
        }else if( role === "driver"){
            return this.driver
        }else{
            return true
        }
    }

    getUserRole = ()=>{
        
        if(this.admin === true){

            return "admin";

        }else if (this.driver === true){

            return "driver";

        }else{

            return "user";

        }
    }

    setUserRole = (role)=>{

        if(role === "admin"){
           
            this.admin = !this.admin ;
            this.driver = false ; 

        }
        else if(role === "driver"){
           
            this.admin = false ;
            this.driver = !this.driver ; 

        }else{
            
            this.admin = false ;
            this.driver = false ; 

        }
    }

    updateRoleToDB = (user,role,dispatch,driver,admin)=>{
        
        return privilageService(user,role,dispatch,driver,admin);

    }


}

export default privilageManager;