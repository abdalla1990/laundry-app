/*
 * filename: table.js
 * Get the order data from store and show
 * */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import Modeedit from 'material-ui/svg-icons/editor/mode-edit';
import CircularProgress from 'material-ui/CircularProgress';
import ModeDelete from 'react-icons/lib/md/delete-forever';
import moment from 'moment';
import {getOrders} from '../../../add-edit-order-handler/addEditOrderHandler'
/**
 * A simple table demonstrating the hierarchy of the `Table` component and its sub-components.
 */
class OrderTable extends Component {

  constructor(props){
    super(props);
    this.state = {
      disabled : undefined,
      orders : undefined,
      disableTable : true
    }
   
         
  }

  componentWillMount = ()=>{
    console.log('orders props : ', this.props)
    console.log('orders state : ', this.state)
    let record = [] ;
    if(this.props.orders === undefined){
      getOrders().then((res)=>{
        if(res.status === 200){
          let orders = res.orders ;
          console.log('orders ############',orders);
          if (orders !== undefined  && orders.length !== 0){
            this.setState(()=>({orders,disableTable:false}));
            orders.map((order,index)=>{
              record.push({index : index , id : order.id, disabled : true });
            });}
        
            console.log('record Array in DidMount: ', record);
            this.setState(()=>{
              return {
                disabled : record
              }
            })
            console.log('disabled Array in DidMount: ', this.state.disabled);
        }
       
      })
    }else{
      console.log('PROPS AND STATE IN ORDERS : ', this.props , this.state);
      this.setState(()=>({orders:this.props.orders,disableTable:false}));
      let record = [];
      this.props.orders.map((order,index)=>{
        record.push({index : index , id : order.id, disabled : true });
      });
      
  
      console.log('record Array in DidMount: ', record);
      this.setState(()=>{
        return {
          disabled : record
        }
      })
      console.log('disabled Array in DidMount: ', this.state.disabled);
    }
  
  }
  handelRemove = (order)=>{
    console.log(' i should edit ',order , "and props r : ", this.props);
    // should warn the order first ! 
     this.props.push('#add_orders',order,'remove');
   }

  handelEdit = (order)=>{
   console.log(' i should edit ',order , "and props r : ", this.props);
    this.props.push('#add_orders',order,'Edit');
  }
  handeCoordinates = (order)=>{
    console.log(' order clicked is : ', order);
    this.props.selection({order});
  }
  onRowSelection = (selectedRow)=>{
    console.log('i am selected : ', selectedRow);
    let record = [];
    this.state.disabled.map((row,index)=>{
    console.log('i am in round number ', index);
      if(row.index === selectedRow[0]){
        console.log('i am inside if')
        record.push({index : row.index , id : this.state.orders[row.index]._id, disabled : false })
      }else {
        console.log('i am inside else')
        record.push({index : row.index , id : this.state.orders[row.index]._id, disabled : true })
      }

    })
    console.log('record Array in selection: ', record);
    this.setState(()=>({disabled:record}))
    console.log('disabled Array in Selection: ', this.state.disabled);
    
  }
  showTableData () {
    if(this.state.orders !== undefined && this.state.orders.length !== 0 && this.state.disabled !== undefined && this.state.disabled.length !== 0){
      return this.state.orders.map((order,index) => {
        return (
          <TableRow key={order.id}>
            <TableRowColumn>{order.serviceType}</TableRowColumn>
            <TableRowColumn>{order.quantityType}</TableRowColumn>
            <TableRowColumn>{order.amount}</TableRowColumn>
            <TableRowColumn>{order.status}</TableRowColumn>
            <TableRowColumn>{moment(Number(order.createdAt)).format("MMM Do YY")}</TableRowColumn>
            <TableRowColumn style={{paddingLeft:"0px"}}>
            
            {this.props.access !== 'driver' && <div className="col-sm-12">
              <FlatButton  disabled={this.state.disabled[index].disabled} onClick={()=>{this.handelEdit(order)}} icon={<Modeedit style={{padding: "1px"}}/>} />
              <FlatButton  disabled={this.state.disabled[index].disabled} onClick={()=>{this.handelRemove(order)}} icon={<ModeDelete style={{padding: "1px",fontSize: '20px'}}/>} />
            </div>
            }
            {this.props.access === 'driver' && <div className="col-sm-12">
              <FlatButton  disabled={this.state.disabled[index].disabled} onClick={()=>{this.handeCoordinates(order)}}label={"Coordinates"} icon={<Modeedit style={{padding: "1px"}}/>} />
            </div>
            }
            
            </TableRowColumn>
          </TableRow>
        );
      });
    }else { 
     return <div>No Data Available</div> 
    }
    
  }

  render() {
    
    return (
      <Table className="table"
      onRowSelection={this.onRowSelection}
      adjustForCheckbox={false}
      showCheckboxes={false}
      >
        <TableHeader adjustForCheckbox={false} showCheckboxes={false}>
          <TableRow adjustForCheckbox={false} showCheckboxes={false}>
            <TableHeaderColumn>service</TableHeaderColumn>
            <TableHeaderColumn>quantity</TableHeaderColumn>
            <TableHeaderColumn>amount</TableHeaderColumn>
            <TableHeaderColumn>Status</TableHeaderColumn>
            <TableHeaderColumn>Created At</TableHeaderColumn>
            <TableHeaderColumn>Actions</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
        {this.state.disableTable ? <CircularProgress  size={80} thickness={5}/> :this.showTableData()}
        </TableBody>
      </Table>
    );
  }
}



export default connect()(OrderTable);
