import React from 'react';



import {connect} from 'react-redux';
import {addUser,editUser,removeUser} from '../../actions/users';
import {editOrder,addOrder,removeOrder,fetchUserOrders} from '../../actions/orders'
import {addUserHandler,editUserHandler,removeUserHandler} from '../../add-edit-user-handler/addEditUserHandler'
import {addOrderHandler,editOrderHandler,removeOrdersHandler} from '../../add-edit-order-handler/addEditOrderHandler'


import classNames from 'classnames';
import { Scrollbars } from 'react-custom-scrollbars';
import Drawer from 'material-ui/Drawer';
import RoutesList from './RoutesList';
import FloatingActionButton from 'material-ui/FloatingActionButton';


import Usertable from './AdminDashboardSlides/Usertable';
import UserForm from './AdminDashboardSlides/UserForm';

import OrderTable from './AdminDashboardSlides/OrderTable';
import OrderForm from './AdminDashboardSlides/OrderForm';

import Dashboard from './AdminDashboardSlides/Dashboard';
import Privilage from './AdminDashboardSlides/Privilage';
class AdminDashboardMaterial extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            renderedSlide : '#dashboard',
            users : this.props.users,
            orders: this.props.orders,
            menuOpen : true,
            state : undefined,
            email:undefined,
            password : undefined
        }
        
  
       
    }
    componentDidUpdate = ()=>{
       
        if(this.props.location.hash){
            
            this.setState((previous)=>{

                if(!previous.renderedSlide){
                    return{
                        renderedSlide :this.props.location.hash
                    }
                }else if(previous.renderedSlide){
                    if(previous.renderedSlide !== this.props.location.hash){
                        return {
                            renderedSlide :this.props.location.hash
                        }
                    }
                }
            })
        }
    
    }
    
   
    changeRenderedSlide = (hash)=>{
        this.props.location.hash = hash ; 
        console.log('the hash in location  after changing is : ',this.props.location.hash);
        this.setState(()=>{
            return {
                renderedSlide : this.props.location.hash,
            }
        })
    }
    
    RnderSlide = () =>{
        if(this.state.renderedSlide !== undefined){
            
            switch(this.state.renderedSlide){
                case '#list_users' : 
                    return this.ListUsers();
                case '#add_users' : 
                    return this.AddUsers();
                case '#list_orders' : 
                    return this.ListOrders();
                case '#system_settings' : 
                    return this.system_settings();
                case '#dashboard' : 
                    return  this.dashboard() ;
                case '#privilage_users':
                    return this.privilage();
                case '#add_orders':
                    return this.AddOrders();
            }
            
        }
    }

    privilage = ()=>{
        
        return(
            <div className="slide">
                <Privilage user ={this.state.state.data}/>
            </div>
        )
    }
        
    
    dashboard = ()=>(
        <div className="slide">
            <Dashboard />
        </div>
        
    )
    ListOrders=()=>{return(
        <div className="slide">
            <OrderTable orders={this.props.orders} access={'admin'} push={this.handelPushFromTabel}/>
        </div>
    )}

        
    
    handelPushFromTabel = (hash,data,mode) =>{
         console.log(' i am back to the admin and the hash is : ', hash, "and data is : ", data , "and mode is : ", mode);
        // console.log('the hash in location is : ',this.props.location.hash);
        this.props.location.hash = hash ; 
        // console.log('the hash in location  after changing is : ',this.props.location.hash);
        let action = mode
        this.setState(()=>{
            return {
                renderedSlide : hash,
                state : {mode:action,
                        data: data}
            }
        })
    }

    ListUsers = ()=>(
        <div className="slide">
            <Usertable admin= {this.props.admin} push={this.handelPushFromTabel}/>
        </div>
         )
    
    AddUsers = ()=>(
        <div className="slide">
           <UserForm state={this.state.state? this.state.state.mode : this.setState(()=>{return{state:{mode:'add'}}})} 
           user={this.state.state?this.state.state.data :undefined} 
           onSubmit ={(user)=>{
            console.log(' in Admin after editing the user (user) :',user);
            console.log(' the state now is : ', this.state.state.mode);
            if(this.state.state.mode === 'add'){
                addUserHandler(user).then((res)=>{
                    this.props.dispatch(addUser(res.user));
                    this.changeRenderedSlide('#list_users');
                }).catch((err)=>{
                    console.log('error : ', err)
                })
                
            }else if (this.state.state.mode === 'edit'){
                console.log('user ######',user);
                editUserHandler(user).then((res)=>{
                    if(res.status === 200){
                        this.props.dispatch(editUser(user.id,user));
                        this.changeRenderedSlide('#list_users');
                    }
                }).catch((err)=>{console.log('error : ', err)});
                
            }else if (this.state.state.mode === 'remove'){
                console.log(' will remove ') ;
                let auth = undefined;
                this.props.admin.map((admin)=>{
                    console.log(' inside loop admin ', admin)
                    if(admin.validate === true){
                        console.log(' removeing ',user,admin);
                        auth = admin.auth ;
                    }
                })
                if(auth !== undefined){
                    removeUserHandler(auth,user._id).then((res)=>{
                        if(res.status === 200){
                            this.props && this.props.dispatch(removeUser(user))
                            this.props && this.changeRenderedSlide('#list_users')
                        }
                    }).catch((err)=>{console.log('error : ', err)});
                    
                }
                
                
            }
           
            }}
            onUnMount = {()=>{
                this.setState(()=>{
                    return {
                        state : undefined
                    }
                })
            }}
            />
        </div>
    )

    AddOrders = ()=>(
    <div className="slide">
        <OrderForm state={this.state.state? this.state.state.mode : this.setState(()=>{return{state:{mode:'add'}}})} 
        order={this.state.state?this.state.state.data :undefined} 
        onSubmit ={(order)=>{
        console.log(' in Admin after editing the Order (order) :',order);
        console.log(' the state now is : ', this.state.state.mode);
            if(this.state.state.mode === 'add'){
                // addOrderHandler(order).then((res)=>{
                //     this.props.dispatch(addOrder(res.order));
                //     this.changeRenderedSlide('#list_orders');
                // }).catch((err)=>{
                //     console.log('error : ', err)
                // })
                
            }else if (this.state.state.mode === 'Edit'){
                console.log('order ######',order);
                let auth = undefined;
                this.props.admin.map((admin)=>{
                    console.log(' inside loop admin ', admin)
                    if(admin.validate === true){
                        console.log(' editing ',admin);
                        auth = admin.auth ;
                    }
                })
                if(auth !== undefined){
                    order.access = 'admin';
                    order.auth = auth;
                    
                    editOrderHandler(order).then((res)=>{
                        if(res.status === 200){
                            this.props.dispatch(editOrder(order.id,order));
                            this.props.dispatch(fetchUserOrders());
                            this.changeRenderedSlide('#dashboard');
                        }
                    }).catch((err)=>{console.log('error : ', err)});
                    
                }
                
            }else if (this.state.state.mode === 'remove'){
                console.log(' will remove ') ;
                let auth = undefined;
                this.props.admin.map((admin)=>{
                    console.log(' inside loop admin ', admin)
                    if(admin.validate === true){
                        console.log(' removeing ',admin);
                        auth = admin.auth ;
                    }
                })
                if(auth !== undefined){
                    

                    // THE AUTH SENT IS THE ADMIN's AUTH HOWEVER THE API IS SET TO EXPECT A USER'S AUTH
                    // FIGUR OUT A WAY TO REDIRECT THE ROUTE TO AUTHENTICATE_ADMIN INSTEAD.

                    // removeOrdersHandler(auth,order._id).then((res)=>{
                    //     console.log('response', res);
                        
                    //     if(res.status === 200){
                    //         console.log('order',order);
                    //         order.auth = user.auth;
                    //         this.props.dispatch(removeOrder({id:this.props.order._id}));
                    //         props.dispatch(fetchUserOrders());
                    //         props.history.push('/');
                    //     }else {
                    //         console.log(res.data);
                    //         this.props.history.push({pathname:'/edit/'+order._id,message:res.data.toString(),order:order});

                    //     }

                    //     }).catch((err)=>{
                    //         console.log('err',err);
                    //         this.props.history.push({pathname:'/edit/'+order._id,message:err.toString(),order:order});
                    //     })
                    
                }
                
                
            }
        
        }}
        onUnMount = {()=>{
            this.setState(()=>{
                return {
                    state : undefined
                }
            })
        }}
        />
    </div>
    )
   
       
       
    system_settings = ()=>(
        <div className="slide">
            System Settings
        </div>
            )    

     
  
    render(){
       
        const HeaderLogoWithMenu = () => (
            <div className="dash-header">
                <h2>Dashboard</h2>
            </div>
        );  

          
        return (
            <div className="dashboard-container">

                <div className="dashboard-header" >
                    <HeaderLogoWithMenu/>
                </div>
                <div className="top-menu-custom">
                    <RoutesList />
                </div>
                <div className="dashboard">
                    {this.state.renderedSlide?
                    <this.RnderSlide  />: undefined}
                </div>
            </div>
            
        )
    }



}

let mapPropsToState = (state)=>{

    return {
        users : state.users,
        orders : state.driverOrders,
        admin : state.admin
    }
}

export default connect(mapPropsToState)(AdminDashboardMaterial) ;