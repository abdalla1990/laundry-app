import React from 'react'; 
import uuid from 'uuid'
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class AdminLogInForm extends React.Component{
    
    constructor(props){
        super(props);
         this.state = {
           password     : '',
           email        : ''
         } 

    }
      
    
    onFormSubmit = (e)=>{
        e.preventDefault();
       
        if (!this.state.email.includes('@')){
            this.setState(()=>{
                return{
                    error :'please enter a valid email'
                }
            })

        } else if(!this.state.password){
            console.log('dont match')
            this.setState(()=>{
                return{
                    error :'password can\'t be empty ' 
                }
            })
        } else {
            this.props.onSubmit({
                password : this.state.password,
                email : this.state.email
            })
        }
            
            
        
            
        
    }  
    onEmailchange=(e)=>{
        const em = e.target.value ; 
        this.setState(()=>({
           
            email : em
        }))
        console.log('state after email : ', this.state);
    }
    onPasschange = (e)=>{
        const pass = e.target.value ; 
        this.setState(()=>({
            
            password : pass
        }))
        console.log('state after pass1 : ', this.state);
    }

    FormLayoutWithLabel = () => (
        <div className="form-container">
        <div className="error-container">{this.state.error}</div>
          <form className="readmin-form" onSubmit={this.onFormSubmit}>
            <div className="row">
              <div className="col-sm-12">
                <div className="row  align-items-center">
                  <div className="col-md-3">
                    <label>Email</label>
                  </div>
                  <div className="col-md-8">
                    <TextField
                      id="email"
                      value={this.state.email} 
                      onChange={this.onEmailchange}
                      fullWidth={true}
                    />
                  </div>
                </div>
      
              </div>
              <div className="col-sm-12">
              <div className="row align-items-center">
                <div className="col-md-3">
                  <label>Password</label>
                </div>
                <div className="col-md-8">
                  <TextField
                    id="password"
                    type="password"
                    value={this.state.password} 
                    onChange={this.onPasschange}
                    fullWidth={true}
                  />
                </div>
              </div>
      
              </div>
              <div className="col-sm-12 pt20">
                
                <button className="button-primary" type="submit">login</button>
                  
              </div>
            </div>
          </form>
        </div>
      );
    render () {
        
        return (<this.FormLayoutWithLabel />);
       
    }
       
    
}

export default AdminLogInForm;