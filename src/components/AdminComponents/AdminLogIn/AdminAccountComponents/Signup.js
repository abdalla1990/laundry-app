import React from 'react';
import AdminAccountForm from './UserAccountForm';
import {connect} from 'react-redux';
import {addUser} from '../../actions/users'
const Signup = (props)=>{
    return (
        <div>
            <p>Sign Up</p>
            <AdminAccountForm 

            onSubmit={(user)=>{
                props.dispatch(addUser(user));
                console.log('user :',user);
                 props.history.push('/login');
            }}
            />
        </div>
    )
}

export default connect()(Signup);