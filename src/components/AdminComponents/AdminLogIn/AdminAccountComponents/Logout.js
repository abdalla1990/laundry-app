import React from 'react';
import AdminAccountForm from './AdminAccountForm';
import {connect} from 'react-redux';
import {validateAdmin} from '../../../../actions/admin'
import AdminLogInForm from './AdminLogInForm'
const Logout = (props)=>{
    return (
        <div>
            <p>Admin Log out</p>
            <AdminLogInForm 

            onSubmit={(AdminCredentials)=>{
                console.log('userCredentials',AdminCredentials);
                props.dispatch(validateAdmin(AdminCredentials));
                
                // props.history.push('/');
            }}
            />
        </div>
    )
}

export default connect()(Logout);