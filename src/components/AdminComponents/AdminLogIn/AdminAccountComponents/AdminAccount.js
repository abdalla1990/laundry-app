import React from 'react';
import {connect} from 'react-redux';
import Adminform from './AdminAccountForm'
import AdminAccountForm from './AdminAccountForm';
import {editUser} from '../../actions/users';
class AdminAccount extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            firstName : this.props.user.firstName || '',
            lastName : this.props.user.lastName || '',
            email : this.props.user.email || '',
            password : this.props.user.password || '',
            showEditForm :false,
            message : ''
        }
    }
    
    onEditClick = (e)=>{
        e.preventDefault();
        console.log(' i am clicked');
        this.setState((previous)=>{
            return {showEditForm : !previous.showEditForm}
        })
    }
    
    componentDidUpdate = ()=>{
        if(this.state.message !== ''){
            setTimeout(()=>{
                this.setState(()=>{
                    return{
                        message : ''
                    }
                })
            },5000)
            
        }
        
    }
    render (){

console.log('props in account :' , this.props);

const editForm = ()=>{

    return (
        <div>
            {this.state.message}
            <AdminAccountForm 
            {...this.props.user} 
            onSubmit={(user)=>{
                console.log('user before send to editing :',user);
                console.log('user id : ', this.props.user.id);
                this.props.dispatch(editUser(this.props.user.id,user));
                console.log('user  after editing :',user);
                this.setState(()=>{
                    return{
                        firstName : user.firstName,
                        lastName : user.lastName,
                        password : user.password,
                        email : user.email,
                        message : 'profile updated successfully'
                    }
                })
                
            }}/>
        </div>
    )
}

        return (
            <div>
            
                <p>Admin Account</p>
               
                <div>
                    
                    <label htmlFor="fname">First Name</label>
                    <p>{this.state.firstName}</p>
                    <label htmlFor="lname">Last Name</label>
                    <p>{this.state.lastName}</p>
                    <label htmlFor="ename">Email</label>
                    <p>{this.state.email}</p>
                   
                </div>
                <div>
                     <button onClick={this.onEditClick}>Edit</button>
                </div>

                {this.state.showEditForm === true && editForm()}
           
            </div>
        )
    }
}

const MapStateToProps = (state)=>{ 
   
        let user =  state.users.find((user)=>user.validate === true);
        return {user};
    
   
}
export default connect(MapStateToProps)(AdminAccount) ;

