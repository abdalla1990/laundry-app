import React from 'react';
import {connect} from 'react-redux';
import {validateAdmin,addAdmin} from '../../../../actions/admin';
import {validateUser,addUser} from '../../../../actions/users';
import AdminLogInForm from './AdminLogInForm';
import axios from 'axios';
const Login = (props)=>{
    
    return (
        <div className="login-form-container">

        <div className="error-container">{props.location.message}</div>
            
            <AdminLogInForm 

            onSubmit={(userCredentials)=>{
              console.log('admin login', props);
                   /*
                   

                   NOTE : 
                   you have to check why it is not saving the AUTH in users' table in localstoragewhich causes the header not to recognize the user/admin validation
                   
                   
                   */ 
                    axios.post('https://mighty-oasis-35468.herokuapp.com/admin/login',userCredentials).then((res)=>{
                        console.log('response', res);
                    if(res.status === 200){
                        console.log('userCredentials',userCredentials,'res auth',res.headers.auth);
                        let AdminAuth =res.data.auth;
                        let UserAuth =res.data.auth;
                        userCredentials = {...userCredentials , AdminAuth}
                        // props.dispatch(addAdmin({
                        //     email:userCredentials.email,
                        //     password:userCredentials.password,
                        //     AdminAuth:AdminAuth,
                        //     firstName:'admin',
                        //     lastName:'admin',
                        //     role : 'admin'
                        // }));
                        // props.dispatch(addUser({
                                                
                        //     email:userCredentials.email,
                        //     password:userCredentials.password,
                        //     UserAuth:AdminAuth,
                        //     firstName:'admin',
                        //     lastName:'admin',
                        //     role: 'admin'
                        // }));
                        props.dispatch(validateAdmin(userCredentials));
                        props.dispatch(validateUser(userCredentials));
                        props.history.push({pathname:'/admin/AdminDashboard',userCredentials:userCredentials});
                    
    
                    }else {
                        console.log(res);
                        props.history.push({pathname:'/admin',message:res.data.toString()});
                        
                    }
                    
                    }).catch((err)=>{
                        console.log('err',err);
                        props.history.push({pathname:'/admin',message:err.toString()});
                    })
                       
               
            }}
            />
        </div>
    )
}

export default connect()(Login);