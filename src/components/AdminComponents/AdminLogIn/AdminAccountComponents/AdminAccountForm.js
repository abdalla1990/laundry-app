import React from 'react'; 
import uuid from 'uuid'


class AdminAccountForm extends React.Component{
    
    constructor(props){
        super(props);
         this.state = {
           id           : this.props.id || uuid.v4(),
           error        :                        '',
           firstName    : this.props.firstName|| '',
           lastName     : this.props.lastName || '', 
           password     : this.props.password || '',
           password2    : this.props.password2|| '',
           email        : this.props.email    || ''
         } 

    }
       
    onFormSubmit = (e)=>{
        e.preventDefault();
        if(this.state.password !== this.state.password2){
            console.log('dont match')
            this.setState(()=>{
                return{
                    error :'passwords dont match'
                }
            })
        }else if (!this.state.email.includes('@')){
            this.setState(()=>{
                return{
                    error :'please enter a valid email'
                }
            })

        } else {
            this.props.onSubmit({
                id : this.state.id,
                firstName:this.state.firstName,
                lastName:this.state.lastName,
                password : this.state.password,
                email : this.state.email
            })
            
        }
            
        
    }
    onReset = ()=>{
       
        this.setState(()=>{
            return {
                id           : this.props.id || uuid.v4(),
                error        : '',
                firstName    : '',
                lastName     : '', 
                password     : '',
                password2    : '',
                email        : ''
            }
        })
    }
    onFNchange = (e)=>{
        const fn = e.target.value ; 
        this.setState(()=>({
            error : '',
            firstName : fn
        }))
        console.log('state after first name : ', this.state);
    } 
    onLNchange=(e)=>{
        const ln = e.target.value ; 
        this.setState(()=>({
            error : '',
            lastName : ln
        }))
        console.log('state after last name : ', this.state);

    }    
    onEmailchange=(e)=>{
        const em = e.target.value ; 
        this.setState(()=>({
            error : '',
            email : em
        }))
        console.log('state after email : ', this.state);
    }
    onPasschange = (e)=>{
        const pass = e.target.value ; 
        this.setState(()=>({
            error : '',
            password : pass
        }))
        console.log('state after pass1 : ', this.state);
    }
    onPass2change = (e)=>{
        const pass2 = e.target.value ; 
        this.setState(()=>({
            error : '',
            password2 : pass2
        }))
        console.log('state after pass2 : ', this.state);
    }
    render () {
        console.log(' my props (useraccountform ) :', this.props);
        return (<div className="form-container">
            <p>{this.state.error}</p>
            <form onSubmit={this.onFormSubmit}>
                <div>
                    <input type="text" placeholder="first Name" value={this.state.firstName} onChange={this.onFNchange}/>
                    <input type="text" placeholder="last Name" value={this.state.lastName} onChange={this.onLNchange}/>
                    <input type="text" placeholder="email" value={this.state.email} onChange={this.onEmailchange}/>
                    <input type="text" placeholder="password" value={this.state.password} onChange={this.onPasschange}/>
                    <input type="text" placeholder="confirm password" value={this.state.password2} onChange={this.onPass2change}/>
                </div>
                <div >
                     <button style={{textAlign : "center",margin: '10px'}}>Submit</button>
                </div>
            </form>

                <div >
                     <button style={{textAlign : "center",margin: '10px'}} onClick={this.onReset}>Reset</button>
                </div>
           
            </div>)
       
    }
       
    
}

export default AdminAccountForm;