import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import AdminNavComponent from './AdminNav/AdminNavComponent';
import {connect} from 'react-redux';
class RoutesList extends Component {

    constructor(props){
        super(props);
        this.state = {
            items : [
                { id : 0 , divider: true, label: 'Admin' , toggled : false},
                { label: 'Dashboard', value: '#dashboard' ,ref:'Admin'    },
                { id : 1 ,divider: true, label: 'Users' , toggled : false },
                { label: 'List Users',value: '#list_users',ref:'Users'    },
                { label: 'Add Users' ,value: '#add_users' ,ref:'Users'    },
                { id : 2 ,divider: true, label: 'Orders' ,toggled : false },
                { label: 'All Orders',value: '#list_orders',ref:'Orders'  },
                { id : 3 ,divider: true, label: 'Settings' , toggled:false},
                {label:'Settings',value: '#system_settings',ref:'Settings'}
            ]
        }
    }

    
  render() {
    
    const filteredItems = this.state.items.filter((item)=>item.divider !== undefined);
    return (
   
        <div className="drawer">
           {filteredItems.map((item)=>(
            <div style={{width: "10%"}} key={item.id}>
                <AdminNavComponent item={item} items={this.state.items} toggled={this.props.menuState[item.id]}/> 
            </div>
           ))}
        </div>
    

    );
  }
}

let mapStateToPRops = (state)=>{
    return {
        menuState : state.menu
    }
}

export default connect(mapStateToPRops)(RoutesList);