
import {BrowserRouter,Route, Switch } from 'react-router-dom'
import {Redirect} from 'react-router';
import React from 'react';
import {connect} from 'react-redux'
import getvalidatedUser from '../selectors/users';
import getvalidatedAdmin from '../selectors/admin';
import LaundryDashboard from '../components/DashboardComponents/LaundryDashboard'
import MakeOrder from '../components/OrderComponents/MakeOrder'
import NotFound from '../components/AssetsComponents/NotFound'
import EditOrder from '../components/OrderComponents/EditOrder'
import Help from '../components/AssetsComponents/Help'
import Header from '../components/AssetsComponents/Header'
import Signup from '../components/AccountComponents/Signup'
import Login from '../components/AccountComponents/Login'
import OrderConfirmation from '../components/OrderComponents/OrderConfirmation'
import Account from '../components/AccountComponents/Account';
import Home from '../components/AssetsComponents/Home';
import AdminLogin from '../components/AdminComponents/AdminLogIn/AdminAccountComponents/Login'
import AdminDashboardMaterial from '../components/AdminComponents/AdminDashboardMaterial'
import Driver from '../components/DriverComponents/Driver';

/*
 
the way Router works is that it goes inside the router and finds the first match letter of the route url ex. /create and / are equal 
in react. so we have to set exact = true in the props for react to match the exact same given url. 

BROWSERROUTER : Is the main directory component API which looks for rouets 
SWITCH : Is used to look for the specific route and if nothing matches it will show the component with no path prop
Routes : We render it to the DOM so that it goes to the routes before anything else and then the router will chose which component to
render based on the URL .
 */

const AppRouter = (props) => {
    
    let PublicProps = props ;
    
    return (
    <BrowserRouter>
        <div className="app-container">
            <Header/>
            <Switch className="app-body">
               
                
                <Route  path="/admin" component={AdminLogin} exact={true} />
                <Route  path="/admin/AdminDashboard" component={AdminDashboardMaterial} exact={true} />

                <Route  path="/user/:id" component={LaundryDashboard} exact={true} />
                <Route  path="/MyAccount" render={(props)=>(
                    PublicProps.user.length !== 0 || PublicProps.admin.length !== 0 ?<Account  history={props.history}/>:<Redirect to="/login"/>
                 )} exact/>
                <Route  path="/newuser" component={Signup} exact={true}/> 
                <Route  path="/login" component={Login} exact={true} />

                <Route  path="/" component={Home} exact={true}/> 
                <Route  path="/create" render={(props)=>{console.log('propssss : ', props); return (
                    PublicProps.user.length !== 0 || PublicProps.admin.length !== 0 ?<MakeOrder history={props.history} />:<Redirect to="/login"/>
                 )}} exact />
                <Route  path="/orders" render={(props)=>(
                    PublicProps.user.length !== 0 || PublicProps.admin.length !== 0 ?<LaundryDashboard  history={props.history}/>:<Redirect to="/login"/>
                 )} exact/>
                <Route  path="/edit/:id" component={EditOrder}/>
                <Route path="/OrderConfirmation" component={OrderConfirmation}/>
                
                
                <Route  path="/help" component={Help} />
                <Route  path='/driver' component={Driver} />
                <Route  component={NotFound}/>
            </Switch>
        </div>
    </BrowserRouter>

)} ;
const MapStateToProps = (state)=>{ 
   
    
    
    return {
        user :  getvalidatedUser(state.users),
        admin : getvalidatedAdmin(state.admin)
        };
    }

export default connect(MapStateToProps)(AppRouter);
