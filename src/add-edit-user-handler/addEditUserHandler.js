import axios from 'axios';
export let addUserHandler =(user)=>{

    let promise = new Promise((resolve,reject)=>{

        try { 
            axios.post('https://mighty-oasis-35468.herokuapp.com/users/create-user',user).then((res)=>{
                console.log('response', res);
            if(res.status === 200){
                console.log('given user',res.data);
    
                
                return resolve({status:200,user:res.data})
                
            }else {
                throw res.data;
                
            }
            
            }).catch((err)=>{
                throw err;
            })
          
        }catch(e){
            return reject(e);
        }
    });

    return promise; 
    
}


export let editUserHandler = (user)=>{
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('user', user);
           
    
            axios.post('https://mighty-oasis-35468.herokuapp.com/users/update-profile',user).then((res)=>{
            console.log('response from userHandler ##########################################', res);
            
            if(res.status === 200){
                return resolve({status:200,data:res.data});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}

export let removeUserHandler = (auth,id)=>{
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('user', id , auth);
            
    
            axios.delete('https://mighty-oasis-35468.herokuapp.com/users/remove-user',{data:{_id:id},headers:{'x-auth':auth}}).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}