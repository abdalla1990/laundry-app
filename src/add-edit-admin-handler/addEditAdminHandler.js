import axios from 'axios';

export let findAdminHandler = (email)=>{
    let promise = new Promise((resolve,response)=>{

        try { 
            console.log('email', email);
            axios.get('https://mighty-oasis-35468.herokuapp.com/admin').then((res)=>{
                console.log('response', res);
            if(res.status === 200){
                let adminExist = res.data.find((admin)=>{

                    return admin.email == email
                })
                if(adminExist){
                    console.log('admin found ', adminExist);
                    return resolve({status:200,admin:adminExist})
                }else{
                    console.log('admin  not found ', adminExist);
                    return resolve({status:200})
                }
    
                
                
                
            }else {
                throw res.data;
                
            }
            
            }).catch((err)=>{
                throw err;
            })
          
        }catch(exeption){
            return reject(exception);
        }
    });

    return promise; 
    
}




export let addAdminHandler =(admin)=>{

    let promise = new Promise((resolve,response)=>{

        try { 
            console.log('admin', admin);
            axios.post('https://mighty-oasis-35468.herokuapp.com/admin/create-admin',admin).then((res)=>{
                console.log('response', res);
            if(res.status === 200){
                console.log('given admin',res.data);
    
                
                return resolve({status:200,admin:res.data})
                
            }else {
                throw res.data;
                
            }
            
            }).catch((err)=>{
                throw err;
            })
          
        }catch(exeption){
            return reject(exception);
        }
    });

    return promise; 
    
}


export let editAdminHandler = (admin)=>{
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('admin', admin);
           
    
            axios.post('https://mighty-oasis-35468.herokuapp.com/admin/update-admin',admin).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}

export let deleteAdminHandler = (email)=>{
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('admin', admin);
           
    
            axios.delete('https://mighty-oasis-35468.herokuapp.com/admin/delete/'+email).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200,message:res.data});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}

export let getUsersHandler = (auth)=>{
    


    let promise = new Promise((resolve,reject)=>{

        try {
        
            axios.get('https://mighty-oasis-35468.herokuapp.com/users',{headers: {'x-auth':auth}}).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200,data:res.data});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}