const usersDefaultState = [ {
        firstName    : 'default',
        lastName     : 'admin', 
        password     : '123456',
        email        : 'default@admin.com',
        validate     : false ,
        }  ];


export default (state = usersDefaultState, action) => {
    switch (action.type) {
        case 'ADD_ADMIN':
            return [...state, action.user];


        case 'REMOVE_ADMIN':
            return state.filter((admin) => {
                return admin.email !== action.admin.email; // this is the only condition that will not return a value 
            });
            
        case 'EDIT_ADMIN':
            return state.map((user) => {
                if (user.id === action.id) {
                    // using spread operator with objects >> { ...Obj , newKey or override existing key} returns the same object with overrided values or new values 
                    return {...user,
                        ...action.updates
                    }

                } else {
                    return user
                }
            })

            case 'VALIDATE_ADMIN':{
                console.log('state : ', state)
                return state.map((admin) => {
                
                    if (admin.email === action.userCredintials.email) {
                        // using spread operator with objects >> { ...Obj , newKey or override existing key} returns the same object with overrided values or new values 
                        console.log('i am inside');
                        return { ...admin,validate :true,auth : action.userCredintials.AdminAuth}
    
                    } else {
                        return { ...admin,validate :false, auth : undefined}
                    }
                })
    
            }

            case 'LOGOUT_USER':{
                console.log('state : ', state)
                return state.map((user) => {
                
                    if (user.email === action.userCredintials.email) {
                        // using spread operator with objects >> { ...Obj , newKey or override existing key} returns the same object with overrided values or new values 
                        console.log('i am inside');
                        return { ...user,validate :false,auth:undefined}
    
                    } else {
                        return user ;
                    }
                })
    
            }
            
        default:
            return state;
    }

};