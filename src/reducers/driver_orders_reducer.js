import {getDriverOrders} from '../add-edit-order-handler/addEditOrderHandler';


let  driverOrdersReducerDefaultState  = [];
getDriverOrders().then((res)=>{
    console.log('HERE',res)
    for(let a in res.orders){
        driverOrdersReducerDefaultState.push(res.orders[a]);
    }
     
});

export default (state = driverOrdersReducerDefaultState, action) => {
    switch (action.type) {

        
        case 'POST_DRIVER_ORDERS':
            return action.orders;
        
        
        default:
            return state;
    }

};