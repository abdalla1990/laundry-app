const ordersReducerDefaultState = [];


export default (state = ordersReducerDefaultState, action) => {
    switch (action.type) {

        case 'CLEAR_ORDERS':
            return [];

        case 'POST_ORDERS':
            return action.orders;
        
        case 'ADD_ORDER':
            return [...state, action.order];


        case 'REMOVE_ORDER':
        console.log('REMOVING : ', action);
            return state.filter((ex) => {
                return ex.id !== action.order.id; // this is the only condition that will not return a value 
            });
            
        case 'EDIT_ORDER':
            return state.map((order) => {
                console.log('in Edit order : ', order);
                if (order._id === action.id) {
                    // using spread operator with objects >> { ...Obj , newKey or override existing key} returns the same object with overrided values or new values 
                   console.log(' i found it : ', order._id , action.updates);
                    return {...order,
                        ...action.updates
                    }

                } else {
                    return order
                }
            })
        

        default:
            return state;
    }

};