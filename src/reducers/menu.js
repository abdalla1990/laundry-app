const usersDefaultState =
    {'0' : false}
;


export default (state = usersDefaultState, action) => {
switch (action.type) {
    case 'TOGGLE_MENU':
        let preToggledeItem = state[action.id] ;
        let preToggledValue = preToggledeItem == undefined ? false : preToggledeItem;
        let invertedToggleValue = !preToggledValue;
        return{...state,
            
            [action.id] : invertedToggleValue
            
        };


        
    case 'UNTOGGLE_MENU':
        
        return{...state,
            
            [action.id] : false
            
        };


        
    default:
        return state;
}

};