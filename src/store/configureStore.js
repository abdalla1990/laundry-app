import { createStore, combineReducers ,applyMiddleware,compose} from 'redux';
import orders_reducer from '../reducers/orders';
import filters_reducer from '../reducers/filters';
import users_reducer from '../reducers/users';
import admin_reducer from '../reducers/admin';
import menu_reducer from '../reducers/menu';
import driver_orders_reducer from '../reducers/driver_orders_reducer'
import {saveState , loadState} from '../localstorageApi/localstorageApi';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import moment from 'moment';


export default () => {
    //persist data to localStorage
    const state = [];
    if(loadState() !== undefined){
        state.users = loadState().users || [];
        state.admin = loadState().admin || [];
    }
    
    state.filters = {
        text: '',
        sortBy: 'Date',
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month')
    }
    
    
  
    console.log('state in config : ', state );


    // const config = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() ;

    const reducers = combineReducers({
        orders: orders_reducer,
        filters: filters_reducer,
        users : users_reducer,
        admin : admin_reducer,
        driverOrders : driver_orders_reducer,
        menu : menu_reducer
       
    });
    // let store = createStore(reducers,state,applyMiddleware(thunk))
    // const composeEnhancers = config ;
    const store = createStore(reducers, state, composeWithDevTools(applyMiddleware(thunk)));
    
    return store;
};