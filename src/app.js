import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import AppRouter from './Router/AppRouter';
import 'normalize.css/normalize.css'; // this module resets the css browser configurations to the default settings.
import './styles/styles.scss'; // loades the styles file which we configured webpack to read in bwepack config file.
import configureStore from './store/configureStore';
import {addOrder} from './actions/orders';
import {setTextFilter , setSortByAmount, setSortByDate} from './actions/filters';
import getvisiableorders from './selectors/orders';
import {saveState , loadState} from './localstorageApi/localstorageApi';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
const store = configureStore() ;

console.log('state before subscripe',store.getState());
store.subscribe(()=>{
      console.log('state before save : ', store.getState());
      saveState(store.getState());
})

//const visiableorders = getvisiableorders(state.orders,state.filters);

//console.log(visiableorders);
const muiTheme = getMuiTheme({
      palette: {
        primary1Color: '#258df2',
        accent1Color: '#40c741',
      }
    });


   const jsx = ( 
      <Provider store = {store}> 
            <MuiThemeProvider muiTheme={muiTheme}>
                  <AppRouter />
            </MuiThemeProvider>
      </Provider>
)


ReactDOM.render(jsx,document.getElementById('app'));