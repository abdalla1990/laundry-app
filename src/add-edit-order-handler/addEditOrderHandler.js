import axios from 'axios';
export let addOrderHandler =(order)=>{

    let promise = new Promise((resolve,response)=>{

        try { 
            axios.post('https://mighty-oasis-35468.herokuapp.com/orders/create-order',order).then((res)=>{
                console.log('response', res);
            if(res.status === 200){
                console.log('given order',res.data);
    
                
                return resolve({status:200,order:res.data})
                
            }else {
                throw res.data;
                
            }
            
            }).catch((err)=>{
                throw err;
            })
          
        }catch(exeption){
            return reject(exception);
        }
    });

    return promise; 
    
}


export let editOrderHandler = (order)=>{
    if(order.auth === undefined){order.auth=order.user.tokens[0].token;}
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('order', order);
            
    
            axios.post('https://mighty-oasis-35468.herokuapp.com/orders/update-order',order).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}


export let getOrders = (userId)=>{
    let promise = new Promise((resolve,reject)=>{

        try {
        
           
    
            axios.get(`https://mighty-oasis-35468.herokuapp.com/orders/get-orders/${userId}`).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200,orders:res.data});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;
}




export let getDriverOrders = ()=>{
    let promise = new Promise((resolve,reject)=>{

        try {

            axios.get(`https://mighty-oasis-35468.herokuapp.com/orders/all-orders`).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200,orders:res.data});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;
}

export let removeOrdersHandler = (auth,id)=>{
    
    let promise = new Promise((resolve,reject)=>{

        try {
        
            console.log('user', id , auth);
            
    
            axios.delete('https://mighty-oasis-35468.herokuapp.com/orders/remove-orders',{data:{_id:id},headers:{'x-auth':auth}}).then((res)=>{
            console.log('response', res);
            
            if(res.status === 200){
                return resolve({status:200});
               
            }else {
                throw res.data;
                
            }
    
            }).catch((err)=>{
                throw err;
               
            })
        }catch(err){
            return reject(err);
        }
    
    });
    
    return promise;


}