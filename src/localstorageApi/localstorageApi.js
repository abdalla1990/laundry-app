export const loadState = ()=>{
    try { 
        // localStorage.clear();
        const serializedState = localStorage.getItem('state');
        console.log('in localstorage the state after load is : ', serializedState);
        if(serializedState === null){
            return undefined ;
        }else{
          return  JSON.parse(serializedState);
        }
    }catch(exeption){
        return undefined;
    }
}


export const saveState = (state)=>{
    try {
        // localStorage.clear();
        console.log('in localstorage : the state ', state);
        localStorage.setItem('state',JSON.stringify(state));
    }catch(err){
        
    }


}